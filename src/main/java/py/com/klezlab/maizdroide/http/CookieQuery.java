package py.com.klezlab.maizdroide.http;

import android.content.Context;

public interface CookieQuery {
    String provideFile();
    String provideField();
    Context provideContext();
}
