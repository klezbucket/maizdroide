package py.com.klezlab.maizdroide.http;

public interface MultiPartQuery extends DataEntityProvider {
    String provideBoundary();
}
