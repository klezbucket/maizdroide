package py.com.klezlab.maizdroide.http;

public class Entity {
    protected static final String FORMDATA = "form-data";
    private final byte[] data;

    public Entity(byte[] data) {
        this.data = data;
    }

    public String provideContentDisposition() {
        return FORMDATA;
    }

    public byte[] getData() {
        return data;
    }
}
