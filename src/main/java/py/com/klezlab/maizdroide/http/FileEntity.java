package py.com.klezlab.maizdroide.http;

public class FileEntity extends Entity {
    private final String mime;

    public FileEntity(byte[] data,String mime) {
        super(data);

        this.mime = mime;
    }

    public String getMime() {
        return mime;
    }
}
