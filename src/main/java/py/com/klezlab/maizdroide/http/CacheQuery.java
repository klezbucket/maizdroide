package py.com.klezlab.maizdroide.http;

public interface CacheQuery {
    String provideEtag();
    String provideLastModified();
}
