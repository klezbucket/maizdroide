package py.com.klezlab.maizdroide.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import py.com.klezlab.maizdroide.debug.Logcat;

public class QueryString {
    public static QueryStringImpl with(HashMap<String,Entity> map,String charset){
        return new QueryStringImpl(map,charset);
    }

    public static class QueryStringImpl {
        private final String charset;
        private HashMap<String,Entity> map;

        public QueryStringImpl(HashMap<String, Entity> map, String charset) {
            this.charset = charset;
            this.map = map;
        }

        public String encode() {
            try {
                StringBuilder sb = new StringBuilder();

                for(String paramName : map.keySet()){
                    String rawval = new String(map.get(paramName).getData());

                    if(rawval != null) {
                        sb.append(paramName);
                        sb.append("=");
                        sb.append(URLEncoder.encode(rawval, charset));
                        sb.append("&");
                    }
                }

                String dirty = sb.toString();

                if(dirty.length() < 1){
                    return dirty;
                }

                String clean = dirty.substring(0,dirty.length() - 1);
                return clean;
            }
            catch (UnsupportedEncodingException e) {
                Logcat.exception("UnsupportedEncodingException @ QueryStringImpl::encode()",e);
                return "";
            }
        }
    }
}
