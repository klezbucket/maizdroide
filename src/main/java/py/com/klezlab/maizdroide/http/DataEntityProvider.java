package py.com.klezlab.maizdroide.http;

import java.util.HashMap;

public interface DataEntityProvider {
    HashMap<String,Entity> provideData();
}
