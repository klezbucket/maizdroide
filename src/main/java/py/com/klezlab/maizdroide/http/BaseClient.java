package py.com.klezlab.maizdroide.http;

import android.util.Base64;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.persistent.Persistent;
import py.com.klezlab.maizdroide.stream.InputStreamUtils;

abstract public class BaseClient {
    private static final String X_FORM_URLENCODED = "application/x-www-form-urlencoded";
    private static final String MULTIPART_FORM_DATA = "multipart/form-data;boundary=";
    private static final String ACCEPT = "Accept";
    private static final String USER_AGENT = "User-Agent";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String AUTHORIZATION = "Authorization";
    private static final String IF_NONE_MATCH = "If-None-Match";
    private static final String IF_MODIFIED_SINCE = "If-Modified-Since";
    private static final String ETAG = "Etag";
    private static final String LAST_MODIFIED = "Last-Modified";
    private static final String CONTENT_LEN = "Content-Length";
    private static final String SETCOOKIE = "Set-Cookie";
    private static final String COOKIE = "Cookie";
    public static final String UTF8 = "UTF8";
    private final static String CRLF  = "\r\n";
    public static final String BOUNDARY = "*****";
    private final static String HYPHENS = "--";
    public static final int CACHE_HIT_STATUS = 304;

    private HashMap<String,String> header = new HashMap();
    private HttpURLConnection conn;
    private URL netUrl;
    private String response;
    private ByteArrayOutputStream binary;

    public byte[] getBinary(){
        if(binary != null) {
            return binary.toByteArray();
        }

        return null;
    }

    public void binaryFlush() throws IOException {
        if(binary != null){
            binary.flush();
        }
    }

    public enum Method{
        GET, POST, PUT;
    };

    /* Entradas */
    private int readTimeout = 10000;
    private int connectTimeout = 10000;
    private String userAgentHeader = "org.klez.maizdroide";
    private String contentTypeHeader = "text/plain; charset=utf8";
    private String acceptHeader = "*/*";
    private Method method = Method.GET;

    public void setHeader(String name,String value){
        header.put(name,value);
    }

    public String getHeader(String name){
        return header.get(name);
    }

    public void flushHeaders(){
        header.clear();
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public HttpURLConnection getConn() {
        return conn;
    }

    public void setConn(HttpURLConnection conn) {
        this.conn = conn;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public String getUserAgentHeader() {
        return userAgentHeader;
    }

    public void setUserAgentHeader(String userAgentHeader) {
        this.userAgentHeader = userAgentHeader;
    }

    public long getContentLength() {
        final long len = Long.parseLong(getResponseHeader(CONTENT_LEN));
        return len;
    }

    public String getContentTypeHeader() {
        return contentTypeHeader;
    }

    public void setContentTypeHeader(String contentTypeHeader) {
        this.contentTypeHeader = contentTypeHeader;
    }

    public String getAcceptHeader() {
        return acceptHeader;
    }

    public void setAcceptHeader(String acceptHeader) {
        this.acceptHeader = acceptHeader;
    }

    /* Salidas */
    private int statusCode;
    private BufferedInputStream rawResponse;
    private String charset = "UTF-8";
    private int binarySize;
    private boolean timeout;
    private boolean interrupted;

    public boolean isInterrupted() {
        return interrupted;
    }

    public boolean isTimeout() {
        return timeout;
    }

    public int getBinarySize() {
        return binarySize;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public BufferedInputStream getRawResponse() {
        return rawResponse;
    }

    public String read(){
        return response;
    }

    public void close(){
        try {
            if(rawResponse != null) {
                rawResponse.close();
            }

            if(conn != null) {
                conn.disconnect();
            }
        }
        catch (IOException e) {
            Logcat.exception("IOException @ BaseClient::close()", e);
        }
    }

    public void query(){
        try {
            final String url = provideUrl();
            timeout = false;
            Logcat.debug("HttpConnection | URL<" + url + ">");
            Logcat.debug("HttpConnection | ReadTimeout<" + readTimeout + ">");
            Logcat.debug("HttpConnection | ConnectTimeout<" + connectTimeout + ">");

            netUrl = new URL(url);
            conn = (HttpURLConnection) netUrl.openConnection();
            conn.setReadTimeout(readTimeout);
            conn.setConnectTimeout(connectTimeout);

            header.put(USER_AGENT, userAgentHeader);
            header.put(CONTENT_TYPE, contentTypeHeader);
            header.put(ACCEPT, acceptHeader);

            if(this instanceof CacheQuery){
                Logcat.debug("HttpConnection | ETAG");
                etag();
            }

            if(this instanceof PostQuery){
                conn.setRequestMethod("POST");
                Logcat.debug("HttpConnection | POST");
                postHeaders();
            }
            else if(this instanceof DeleteQuery){
                conn.setRequestMethod("DELETE");
                Logcat.debug("HttpConnection | DELETE");
            }
            else if(this instanceof PutQuery){
                conn.setRequestMethod("PUT");
                Logcat.debug("HttpConnection | PUT");
                putHeaders();
            }

            if(this instanceof MultiPartQuery){
                Logcat.debug("HttpConnection | MULTIPART");
                multiPartHeaders();
            }

            if(this instanceof BasicAuth){
                Logcat.debug("HttpConnection | BASIC AUTH");
                basicAuth();
            }

            if(this instanceof CookieQuery){
                Logcat.debug("HttpConnection | COOKIE JAR");
                putCookie();
            }

            setHeaders(conn);

            if(this instanceof PostQuery){
                postWriter();
            }
            else if(this instanceof PutQuery){
                putWriter();
            }

            statusCode = conn.getResponseCode();
            Logcat.debug("HttpConnection | StatusCode<" + statusCode + ">");

            rawResponse = new BufferedInputStream(conn.getInputStream());
            Logcat.debug("HttpConnection | ResponseHeaders<" + conn.getHeaderFields() + ">");

            if(this instanceof CookieQuery){
                saveCookie();
            }

            if(this instanceof Streamer) {
                streamResponse();
            }
            else if(this instanceof Binary) {
                binaryResponse();
            }
            else {
                response = InputStreamUtils.with(rawResponse).toString(charset);

                Logcat.debug("HttpConnection | Response<" + response + ">");
                Logcat.debug("HttpConnection | ResponseSize<" + response.length()  + ">");
                rawResponse.close();
            }
        }
        catch (SocketTimeoutException e) {
            Logcat.exception("SocketTimeoutException @ BaseClient::query()",e);
            e.printStackTrace();
            rawResponse = null;
            binary = null;
            timeout = true;
        }
        catch (InterruptedIOException e) {
            Logcat.exception("InterruptedIOException @ BaseClient::query()",e);
            e.printStackTrace();
            rawResponse = null;
            binary = null;
            interrupted = true;
        }
        catch (IOException e) {
            Logcat.exception("IOException @ BaseClient::query()",e);
            e.printStackTrace();
            rawResponse = null;
            extractError();
        }
    }

    private void saveCookie() {
        final String setCookie = getResponseHeader(SETCOOKIE);

        if(setCookie == null){
            return;
        }

        final String[] cookies = setCookie.split(";");

        if(cookies.length < 1){
            return;
        }

        final CookieQuery cookieQuery = (CookieQuery) this;
        final String file = cookieQuery.provideFile();
        final String field = cookieQuery.provideField();

        Persistent.with(cookieQuery.provideContext(),file).write(field,cookies[0]);
    }

    protected abstract String provideUrl();

    private void extractError() {
        BufferedInputStream error = new BufferedInputStream(conn.getErrorStream());
        response = InputStreamUtils.with(error).toString(charset);
        Logcat.debug("HttpConnection | ErrorStream<" + response + ">");
    }

    private void binaryResponse() throws IOException {
        Logcat.debug("HttpConnection | BINARY");
        binary = new ByteArrayOutputStream();
        int read;
        byte[] data = new byte[Integer.parseInt(conn.getHeaderField(CONTENT_LEN))];

        while((read = rawResponse.read(data,0,data.length)) != -1){
            binary.write(data, 0, read);
        }

        binarySize = binary.toByteArray().length;
        Logcat.debug("HttpConnection | BinarySize<" + binarySize + ">");
    }


    private void streamResponse() throws IOException {
        Logcat.debug("HttpConnection | STREAM");
    }

    public boolean isBinaryCompleted(){
        return binarySize == Integer.parseInt(getResponseHeader(CONTENT_LEN));
    }

    public String getEtag(){
        return conn.getHeaderField(ETAG);
    }

    public String getLastModified(){
        return conn.getHeaderField(LAST_MODIFIED);
    }

    private void etag() {
        CacheQuery cacheQuery = (CacheQuery) this;
        String etag = cacheQuery.provideEtag();
        String lastModified = cacheQuery.provideLastModified();

        if(etag != null) {
            header.put(IF_NONE_MATCH, etag);
        }

        if(lastModified != null) {
            header.put(IF_MODIFIED_SINCE, lastModified);
        }
    }

    public Map<String, List<String>> getResponseHeaders(){
        return conn.getHeaderFields();
    }

    private void multiPartHeaders() {
        MultiPartQuery formData = (MultiPartQuery) this;

        StringBuilder sb = new StringBuilder()
                .append(MULTIPART_FORM_DATA)
                .append(formData.provideBoundary());

        header.put(CONTENT_TYPE,sb.toString());
    }

    private void postHeaders() {
        header.put(CONTENT_TYPE, X_FORM_URLENCODED);
    }

    private void putHeaders() {
        header.put(CONTENT_TYPE, X_FORM_URLENCODED);
    }

    private void putWriter() throws IOException {
        final PutQuery put = (PutQuery) this;

        if(this instanceof MultiPartQuery){
            multiPartWriter();
            return;
        }

        final String data = QueryString.with(put.provideData(), BaseClient.UTF8).encode();
        conn.setDoOutput(true);

        Logcat.debug("HttpConnection | PUTDATA<" + data + ">");

        DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
        writer.writeBytes(data);
        writer.flush();
        writer.close();
    }

    private void multiPartWriter() throws IOException {
        final MultiPartQuery formData = (MultiPartQuery) this;
        conn.setDoOutput(true);

        DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
        HashMap<String, Entity> entities = formData.provideData();

        if(entities != null){
            final Iterator iterator = entities.entrySet().iterator();

            while(iterator.hasNext()){
                Map.Entry entry = (Map.Entry) iterator.next();
                Entity entity = (Entity) entry.getValue();
                String field = (String) entry.getKey();

                writer.writeBytes(getSeparator(formData));
                putFormData(entity, field, writer);


                byte[] data = entity.getData();
                Logcat.debug("HttpConnection | MULTIPART<" + field + "," + data +">");

                writer.writeBytes(CRLF);
                writer.writeBytes(CRLF);
                writer.write(data);
                writer.writeBytes(CRLF);
            }
        }

        writer.writeBytes(getSeparator(formData));
        writer.writeBytes(HYPHENS);
        writer.writeBytes(CRLF);
        writer.flush();
        writer.close();
    }

    private void putFormData(Entity entity, String field, DataOutputStream writer) throws IOException {
        if(entity instanceof FileEntity){
            putFileData((FileEntity)entity,field,writer);
        }
        else{
            StringBuilder sb = new StringBuilder()
                    .append("Content-Disposition: ")
                    .append(entity.provideContentDisposition())
                    .append(";name=\"")
                    .append(field)
                    .append("\"");

            writer.writeBytes(sb.toString());
        }
    }

    private void putFileData(FileEntity entity, String field, DataOutputStream writer) throws IOException {
        StringBuilder sb = new StringBuilder()
                .append("Content-Disposition: ")
                .append(entity.provideContentDisposition())
                .append(";name=\"")
                .append(field)
                .append("\";filename=\"")
                .append(entity.getMime())
                .append("\"");

        writer.writeBytes(sb.toString());
        writer.writeBytes(CRLF);
        writer.writeBytes("Content-Type: ");
        writer.writeBytes(entity.getMime());
    }

    private String getSeparator(MultiPartQuery formData){
        StringBuilder separator = new StringBuilder()
                .append(HYPHENS)
                .append(formData.provideBoundary())
                .append(CRLF);

        return separator.toString();
    }

    private void postWriter() throws IOException {
        final PostQuery post = (PostQuery) this;

        if(this instanceof MultiPartQuery){
            multiPartWriter();
            return;
        }

        final String data = QueryString.with(post.provideData(), BaseClient.UTF8).encode();
        conn.setDoOutput(true);

        Logcat.debug("HttpConnection | POSTDATA<" + data + ">");

        DataOutputStream writer = new DataOutputStream(conn.getOutputStream());
        writer.writeBytes(data);
        writer.flush();
        writer.close();
    }

    public String getResponseHeader(String key){
        return conn.getHeaderField(key);
    }

    private void setHeaders(HttpURLConnection conn) {
        for (String name : header.keySet()) {
            String value = header.get(name);
            conn.setRequestProperty(name, value);

            Logcat.debug("HttpConnection | HEADER<" + name + "> : " + "<" + value + ">");
        }
    }

    private void putCookie(){
        final CookieQuery cookieQuery = (CookieQuery) this;
        final String file = cookieQuery.provideFile();
        final String field = cookieQuery.provideField();
        final String cookie = Persistent.with(cookieQuery.provideContext(),file).read(field);

        if(cookie == null){
            return;
        }

        header.put(COOKIE,cookie);
    }

    private void basicAuth() {
        BasicAuth auth = (BasicAuth) this;
        StringBuilder val;
        StringBuilder sb;
        byte[] credentials;

        sb = new StringBuilder();
        sb.append(auth.provideUser());
        sb.append(":");
        sb.append(auth.providePassword());

        credentials = sb.toString().getBytes();
        val = new StringBuilder();
        val.append("Basic ");
        val.append(Base64.encodeToString(credentials,Base64.NO_WRAP));

        header.put(AUTHORIZATION,val.toString());
    }
}