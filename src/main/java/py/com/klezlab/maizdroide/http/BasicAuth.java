package py.com.klezlab.maizdroide.http;

public interface BasicAuth {
    String provideUser();
    String providePassword();
}
