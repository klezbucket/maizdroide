package py.com.klezlab.maizdroide.http;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Map;

public abstract class JsonReader extends BaseClient {
    private static final String JSON_MIME = "application/json; charset=utf-8";

    public JsonReader(){
        super();
        setAcceptHeader(JSON_MIME);
    }

    public Map readMap() throws JsonReaderException{
        try {
            Gson gson = new Gson();
            return gson.fromJson(read(), Map.class);
        }
        catch(Exception e){
            throw new JsonReaderException("Exception @ JsonReader::readMap()",e);
        }
    }

    public int readInteger() throws JsonReaderException{
        try{
            Gson gson = new Gson();
            return gson.fromJson(read(),Integer.class);
        }
        catch(Exception e){
            throw new JsonReaderException("Exception @ JsonReader::readInteger()",e);
        }
    }

    public boolean readBoolean() throws JsonReaderException{
        try{
            Gson gson = new Gson();
            return gson.fromJson(read(),Boolean.class);
        }
        catch(Exception e){
            throw new JsonReaderException("Exception @ JsonReader::readBoolean()",e);
        }
    }

    public ArrayList readArrayList() throws JsonReaderException{
        try{
            Gson gson = new Gson();
            return gson.fromJson(read(),ArrayList.class);
        }
        catch(Exception e){
            throw new JsonReaderException("Exception @ JsonReader::readArrayList()",e);
        }
    }

    public class JsonReaderException extends Exception{
        public JsonReaderException(String message,Throwable cause) {
            super(message,cause);
        }

        public JsonReaderException(String message) {
            super(message);
        }
    }
}
