package py.com.klezlab.maizdroide.file;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.Nullable;

import java.io.File;

public class FileManager {
    public static String getPrivatePath(Context context,String fileName){
        if(fileName == null){
            return null;
        }

        final File file = new File(context.getFilesDir(), fileName);
        return file.getAbsolutePath();
    }

    public static String getPrivatePath(Context context, String folder,String fileName){
        if(fileName == null){
            return null;
        }

        final File dir = new File(context.getFilesDir(),folder);
        dir.mkdirs();

        final File file = new File(dir.getAbsolutePath(), fileName);
        return file.getAbsolutePath();
    }

    public static String getPublicPath(Context context,String fileName){
        if(fileName == null){
            return null;
        }

        final File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName);
        return file.getAbsolutePath();
    }

    public static String getPublicPath(Context context,String folder,String fileName){
        if(fileName == null){
            return null;
        }

        final File dir = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),folder);
        dir.mkdirs();

        final File file = new File(dir.getAbsolutePath(), fileName);
        return file.getAbsolutePath();
    }

    @Nullable
    public static String getGallery(String album,String fileName){
        if(fileName == null){
            return null;
        }

        if(album == null){
            return null;
        }

        final File albumDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),album);
        albumDir.mkdirs();

        final File file = new File(albumDir.getAbsolutePath(), fileName);
        return file.getAbsolutePath();
    }
}
