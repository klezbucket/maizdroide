package py.com.klezlab.maizdroide.file;

import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.debug.Logcat;

/**
 * Created by klez on 04/10/16.
 */
public class BitmapReader {
    @Nullable
    public static String resolvMime(String path) {
        final BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);

        Logcat.debug("opts:" + opts.toString());

        return opts.outMimeType;
    }
}
