package py.com.klezlab.maizdroide.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import py.com.klezlab.maizdroide.debug.Logcat;

public class FileReader {
    public static byte[] bufferDump(String path){
        final File file = new File(path);
        final byte[] array = new byte[(int) file.length()];

        final FileInputStream inputStream;

        try {
            inputStream = new FileInputStream(file);
            inputStream.read(array);
            inputStream.close();

            return array;
        }
        catch (IOException e) {
            Logcat.exception("IOException @ FileReader::bufferDump()",e);
        }

        return null;
    }

    public static void copy(File src, File dst) throws IOException {
        final InputStream in = new FileInputStream(src);
        final OutputStream out = new FileOutputStream(dst);

        byte[] buf = new byte[1024];
        int len;

        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        in.close();
        out.close();
    }
}
