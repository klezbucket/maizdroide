package py.com.klezlab.maizdroide.file;

import android.graphics.Bitmap;

import java.io.FileOutputStream;
import java.io.IOException;

import py.com.klezlab.maizdroide.debug.Logcat;

public class BitmapWriter {
    public static BitmapWriterImpl with(String filename){
        return new BitmapWriterImpl(filename);
    }

    public static class BitmapWriterImpl{
        final private String filename;

        public BitmapWriterImpl(String filename) {
            this.filename = filename;
        }

        public void write(Bitmap bitmap){
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(filename);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            }
            catch (Exception e) {
                Logcat.exception("Exception @ BitmapWriter::write()",e);
            }
            finally {
                closeOutputStream(out);
            }
        }

        private void closeOutputStream(FileOutputStream out) {
            if (out == null) {
                return;
            }

            try {
                out.close();
            }
            catch (IOException e) {
                Logcat.exception("IOException @ BitmapWriter::closeOutputStream()",e);
            }
        }
    }
}
