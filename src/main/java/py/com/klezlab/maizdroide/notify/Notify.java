package py.com.klezlab.maizdroide.notify;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.util.HashMap;
import java.util.Iterator;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Notify {
    public static NotifyImpl with(Context ctx){
        return new NotifyImpl(ctx);
    }

    public static class NotifyImpl{
        private final Context context;
        private int smallIcon = android.R.drawable.ic_dialog_alert;
        private String title;
        private String text;
        private Class<?> activity;
        private int id = 0;
        private String group;
        private int icon = android.R.drawable.sym_def_app_icon;
        private HashMap<String,String> extras;
        private boolean vibrate;
        private long[] vibration;
        private boolean ringtone;
        private Uri tone;

        public NotifyImpl(Context ctx){
            this.context = ctx;
        }

        public NotifyImpl setRingtone(Uri tone){
            this.ringtone = true;
            this.tone = tone;

            return this;
        }

        public NotifyImpl setVibrate(long[] vibration){
            this.vibrate = true;
            this.vibration = vibration;

            return this;
        }

        public NotifyImpl setSmallIcon(int smallIcon) {
            this.smallIcon = smallIcon;
            return this;
        }

        public NotifyImpl setTitle(String title) {
            this.title = title;
            return this;
        }

        public NotifyImpl setText(String text) {
            this.text = text;
            return this;
        }

        public NotifyImpl setActivity(Class<?> activity) {
            this.activity = activity;
            return this;
        }

        public NotifyImpl setId(int id) {
            this.id = id;
            return this;
        }

        public NotifyImpl setGroup(String group) {
            this.group = group;
            return this;
        }

        public NotifyImpl setIcon(int icon) {
            this.icon = icon;
            return this;
        }

        public NotifyImpl addExtra(String key,String value) {
            if(extras == null){
                extras = new HashMap<>();
            }

            extras.put(key,value);
            return this;
        }

        public void display(){
            final Intent intent = new Intent(context, activity);
            Logcat.debug("NotifyIntent<activity:" + activity + ">");

            if(extras != null){
                final Iterator iterator = extras.entrySet().iterator();

                while (iterator.hasNext()){
                    HashMap.Entry<String,String> pair = (HashMap.Entry<String, String>) iterator.next();
                    String key = pair.getKey();
                    String value = pair.getValue();
                    intent.putExtra(key,value);

                    Logcat.debug("NotifyExtra<key:" + key + ",value:" + value + ">");
                }
            }

            final PendingIntent pending = PendingIntent.getActivity(context, id, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
            final NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            Logcat.debug("NotifySmallIcon<resId:" + smallIcon + ">");
            builder.setSmallIcon(smallIcon);

            Logcat.debug("NotifyIcon<resId:" + icon + ">");
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon));

            builder.setGroup(group);
            builder.setContentIntent(pending);
            builder.setAutoCancel(true);
            builder.setContentText(text);
            builder.setContentTitle(title);

            if(ringtone){
                Logcat.debug("NotifySound<ringtone:" + tone + ">");
                builder.setSound(tone);
            }

            if(vibrate) {
                Logcat.debug("NotifyVibrate<vibration:" + vibration + ">");
                builder.setVibrate(vibration);
            }

            final Notification notification = builder.build();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(group, id, notification);

            Logcat.debug("NotifyDisplay<group:" + group + ",id:" + id + ",title:" + title + ",text: +" + text + ">");
        }

        public void cancel(){
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(group, id);
            Logcat.debug("NotifyCancel<group:" + group + ",id:" + id + ">");
        }
    }
}
