package py.com.klezlab.maizdroide.date;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;

import py.com.klezlab.maizdroide.debug.Logcat;

public class DateUtil {
    private static final String SLASH = "/";

    public static String today(){
        final Date date = new Date();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static String readableDate(String datetime){
        try {
            final String[] datetimes = datetime.split(" ");
            final String date = datetimes[0];
            final String[] dates = date.split("-");
            final StringBuilder sb = new StringBuilder()
                    .append(dates[2])
                    .append(SLASH)
                    .append(dates[1])
                    .append(SLASH)
                    .append(dates[0]);

            return sb.toString();
        }
        catch (Exception e){
            Logcat.exception("Exception @ DateUtil::readableDate()",e);
            return datetime;
        }
    }

    public static String readableHumanDate(String datetime) {
        try {
            final String[] datetimes = datetime.split(" ");
            final String date = datetimes[0];
            final String[] dates = date.split("-");
            final String fullMonth = new DateFormatSymbols().getMonths()[Integer.parseInt(dates[1]) - 1];
            final StringBuilder month = new StringBuilder()
                    .append(fullMonth.substring(0, 1).toUpperCase())
                    .append(fullMonth.substring(1, 3));

            final StringBuilder sb = new StringBuilder()
                    .append(dates[2])
                    .append(" ")
                    .append(month.toString())
                    .append(" ")
                    .append(dates[0]);

            return sb.toString();
        }
        catch (Exception e){
            Logcat.exception("Exception @ DateUtil::readableHumanDate()",e);
            return datetime;
        }
    }
}
