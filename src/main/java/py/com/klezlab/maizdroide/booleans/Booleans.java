package py.com.klezlab.maizdroide.booleans;

import android.support.annotation.Nullable;

public class Booleans {
    public static final String STRING_TRUE = "true";

    @Nullable
    public static boolean extractor(Object object){
        if(object == null){
            return false;
        }

        boolean value = false;

        if(object instanceof String){
            final String s = (String) object;
            value = Boolean.parseBoolean(s);
        }
        else if(object instanceof Double){
            final double d = (double) object;
            value = d > 0;
        }
        else if(object instanceof Long){
            final long l = (long) object;
            value = l > 0;
        }
        else if(object instanceof Integer){
            final int i = (int) object;
            value = i > 0;
        }
        else if(object instanceof Boolean){
            value = (boolean) object;
        }

        return value;
    }
}