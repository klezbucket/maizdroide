package py.com.klezlab.maizdroide.camera;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;

import java.io.File;

import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.file.BitmapReader;

public abstract class CameraAbstractEngine implements CameraHook {
    private final static String BUNDLE_RESTORE_SHOT = "CameraAbstractEngine.BUNDLE_RESTORE_SHOT";

    private MaizActivity activity;
    private boolean restoreShot = false;
    private Bundle bundle;

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        this.activity = activity;

        if(savedInstanceState == null){
            purge();
            return;
        }

        bundle = savedInstanceState;
        restoreShot();
    }

    public void restoreShot(){
        if(bundle == null){
            return;
        }

        restoreShot =  bundle.getBoolean(BUNDLE_RESTORE_SHOT,false);

        if(restoreShot){
            final File file = new File(providePath());

            if(file.exists()) {
                final Uri shotUri = Uri.fromFile(file);
                atShotSuccess(shotUri);
            }
        }
    }

    public void purge() {
        final File file = new File(providePath());

        if(file.exists()) {
            file.delete();
        }
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {
        outstate.putBoolean(BUNDLE_RESTORE_SHOT,restoreShot);
    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {
        if(requestCode == provideActionId()){
            if(resultCode == Activity.RESULT_OK) {
                final File file = new File(providePath());
                final Uri shotUri = Uri.fromFile(file);
                atShotSuccess(shotUri);
            }
            else{
                atShotFailure();
            }
        }
    }

    public void shot(){
        try {
            final File file = new File(providePath());
            final Uri shotUri = Uri.fromFile(file);
            restoreShot = true;

            final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, shotUri);
            activity.startActivityForResult(intent, provideActionId());
        }
        catch (ActivityNotFoundException e) {
            Logcat.exception("ActivityNotFoundException @ CameraAbstractEngine::shot()", e);
            atCameraNotFound();
        }
    }

    public boolean hasFile(){
        final File file = new File(providePath());

        if(file.exists() == false){
            return false;
        }

        return restoreShot;
    }

    public String provideMime() {
        return BitmapReader.resolvMime(providePath());
    }
}
