package py.com.klezlab.maizdroide.camera;

import android.net.Uri;

import py.com.klezlab.maizdroide.activity.Hook;

public interface CameraHook extends Hook {
    void atCameraNotFound();
    int provideActionId();
    String providePath();
    void atShotFailure();
    void atShotSuccess(Uri shotUri);
}
