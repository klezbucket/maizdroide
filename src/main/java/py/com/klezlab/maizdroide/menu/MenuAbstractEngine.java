package py.com.klezlab.maizdroide.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

import py.com.klezlab.maizdroide.activity.MaizActivity;

public abstract class MenuAbstractEngine implements MenuHook {
    private ActionBarDrawerToggle drawerButton;
    private DrawerLayout drawer;

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {

    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {
        drawerStart(activity);
        menuStart(activity);
    }

    private void menuStart(MaizActivity activity) {
        final int id = provideList();

        if(id == 0){
            return;
        }

        final ListView view = (ListView) activity.findViewById(id);
        final BaseAdapter adapter = provideAdapter(activity);

        if(adapter == null){
            return;
        }

        view.setAdapter(adapter);
    }

    protected void toggleDrawer() {
        if(drawer == null){
            return;
        }

        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            drawer.openDrawer(GravityCompat.START);
        }
    }

    private void drawerStart(MaizActivity activity) {
        final Toolbar toolbar = (Toolbar) activity.findViewById(provideToolbar());
        drawer = (DrawerLayout) activity.findViewById(provideDrawer());
        drawerButton = new ActionBarDrawerToggle(activity, drawer, toolbar, provideDrawerOpenString(), provideDrawerCloseString()) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                atDrawerClosed(view);
            }

            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
                atDrawerOpened(view);
            }
        };

        drawerButton.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atClick();
            }
        });

        final int drawable = provideHomeDrawable();

        if(drawable != 0) {
            drawerButton.setHomeAsUpIndicator(drawable);
            drawerButton.setDrawerIndicatorEnabled(false);
        }

        drawerButton.syncState();

        if (drawer != null) {
            drawer.addDrawerListener(drawerButton);
        }
    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        if (drawerButton.onOptionsItemSelected(item)) {
            return true;
        }

        return false;
    }

    public void openDrawer() {
        if(drawer == null){
            return;
        }

        if(drawer.isDrawerOpen(GravityCompat.START) == false){
            drawer.openDrawer(GravityCompat.START);
        }
    }


    public void closeDrawer() {
        if(drawer == null){
            return;
        }

        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
