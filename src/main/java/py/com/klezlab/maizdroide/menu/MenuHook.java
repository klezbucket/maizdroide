package py.com.klezlab.maizdroide.menu;

import android.view.View;
import android.widget.BaseAdapter;

import py.com.klezlab.maizdroide.activity.Hook;
import py.com.klezlab.maizdroide.activity.MaizActivity;

public interface MenuHook extends Hook {
    void atDrawerOpened(View view);
    void atDrawerClosed(View view);
    int provideDrawerOpenString();
    int provideDrawerCloseString();
    int provideDrawer();
    BaseAdapter provideAdapter(MaizActivity activity);
    int provideList();
    int provideHomeDrawable();
    int provideToolbar();
    void atClick();
}
