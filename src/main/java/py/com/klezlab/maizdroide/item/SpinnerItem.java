package py.com.klezlab.maizdroide.item;

import android.view.View;

public class SpinnerItem implements Comparable {
    private String label;
    private String value;
    private View view;

    public SpinnerItem(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    @Override
    public String toString() {
        return getLabel();
    }

    @Override
    public int compareTo(Object another) {
        SpinnerItem comp = (SpinnerItem) another;
        return getLabel().compareToIgnoreCase(comp.getLabel());
    }
}
