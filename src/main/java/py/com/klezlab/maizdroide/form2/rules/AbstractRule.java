package py.com.klezlab.maizdroide.form2.rules;

import android.support.annotation.Nullable;

public abstract class AbstractRule {
    public abstract String provideValidationMessage();
    public abstract boolean validates(@Nullable String value);
}
