package py.com.klezlab.maizdroide.form2.inputs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRatingBar;
import android.widget.TextView;

import py.com.klezlab.maizdroide.form2.FormBind;

abstract public class RatingBind extends FormBind {
    private final AppCompatRatingBar ratingBar;

    public RatingBind (@NonNull AppCompatRatingBar ratingBar,@NonNull String fieldName, @Nullable TextView validation) {
        super(ratingBar, fieldName, validation);

        this.ratingBar = ratingBar;
    }

    @Nullable
    @Override
    public String read() {
        return String.valueOf(ratingBar.getRating());
    }

    @Override
    public void write(@Nullable String value) {
        ratingBar.setRating(Float.parseFloat(value));
    }
}
