package py.com.klezlab.maizdroide.form2.memento;

public interface PersistentMemento extends Memento {
    String provideFile();
}
