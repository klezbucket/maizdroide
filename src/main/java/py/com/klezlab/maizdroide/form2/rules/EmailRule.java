package py.com.klezlab.maizdroide.form2.rules;

import android.support.annotation.Nullable;

import static android.util.Patterns.EMAIL_ADDRESS;

public abstract class EmailRule extends AbstractRule {
    @Override
    public boolean validates(@Nullable String value) {
        if(value == null){
            return false;
        }

        return EMAIL_ADDRESS.matcher(value).matches();
    }
}
