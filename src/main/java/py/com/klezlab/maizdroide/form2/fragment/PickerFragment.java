package py.com.klezlab.maizdroide.form2.fragment;

import android.app.DatePickerDialog;
import android.support.v4.app.DialogFragment;

import py.com.klezlab.maizdroide.form2.FormBind;

public abstract class PickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener  {
    private FormBind bind;

    public FormBind getBind() {
        return bind;
    }

    public void setBind(FormBind bind) {
        this.bind = bind;
    }
}
