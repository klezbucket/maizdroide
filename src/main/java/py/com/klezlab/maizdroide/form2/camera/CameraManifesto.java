package py.com.klezlab.maizdroide.form2.camera;

import android.view.View;
import android.widget.TextView;

import py.com.klezlab.maizdroide.camera.CameraAbstractEngine;

public class CameraManifesto {
    public View input;
    public TextView validation;
    public String fieldName;
    public CameraAbstractEngine engine;
}
