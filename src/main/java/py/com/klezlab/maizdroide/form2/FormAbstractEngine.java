package py.com.klezlab.maizdroide.form2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;

import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.file.FileReader;
import py.com.klezlab.maizdroide.form2.fragment.PickerFragment;
import py.com.klezlab.maizdroide.form2.inputs.Picker;
import py.com.klezlab.maizdroide.form2.memento.BundleMemento;
import py.com.klezlab.maizdroide.form2.memento.PersistentMemento;
import py.com.klezlab.maizdroide.form2.rules.AbstractRule;
import py.com.klezlab.maizdroide.http.Entity;
import py.com.klezlab.maizdroide.http.FileEntity;
import py.com.klezlab.maizdroide.persistent.Persistent;

public abstract class FormAbstractEngine implements FormHook {
    private HashMap<String,Integer> fieldLookup;

    private ArrayList<FormBind> provideBinds(){
        final ArrayList<FormBind> binds = provideFormBinds();

        if(fieldLookup == null){
            resolvFieldLookup(binds);
        }

        return binds;
    }

    private void resolvFieldLookup(ArrayList<FormBind> binds) {
        fieldLookup = new HashMap<>();

        int i = 0;

        for(FormBind bind : binds){
            fieldLookup.put(bind.getFieldName(),i);
            i++;
        }
    }

    public boolean isValid(boolean displayValidations){
        final ArrayList<FormBind> binds = provideBinds();
        boolean result = true;

        for(FormBind bind : binds){
            result &= validates(bind);
        }

        if(displayValidations){
            toggleValidations();
        }

        return result;
    }

    public boolean isValid(){
        return isValid(true);
    }

    public void toggleValidations() {
        final ArrayList<FormBind> binds = provideBinds();

        for(FormBind bind : binds){
            bind.toggleValidation();
        }
    }

    private boolean validates(FormBind bind) {
        final ArrayList<AbstractRule> rules = bind.provideRules();

        for(AbstractRule rule : rules){
            if(rule.validates(bind.read()) == false){
                bind.invalidate(rule.provideValidationMessage());
                return false;
            }
        }

        bind.validate();
        return true;
    }

    public SparseArray<String> extract(){
        final ArrayList<FormBind> binds = provideBinds();
        final SparseArray<String> data = new SparseArray<>();

        for(FormBind bind : binds){
            data.put(bind.getInput().getId(),bind.read());
        }

        return data;
    }

    public void hideValidations() {
        final ArrayList<FormBind> binds = provideBinds();

        for(FormBind bind : binds){
            bind.hideValidation();
        }
    }

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        final ArrayList<FormBind> binds = provideBinds();

        if(savedInstanceState != null){
            restorePickers(activity);
        }

        for(FormBind bind : binds){
            if(bind instanceof BundleMemento){
                restoreBundle(bind,savedInstanceState);
            }
            else if(bind instanceof PersistentMemento){
                restorePersistent(bind,activity);
            }
        }
    }

    private void restorePickers(MaizActivity activity) {
        final ArrayList<FormBind> binds = provideBinds();
        final FragmentManager manager = activity.getSupportFragmentManager();

        for(FormBind bind : binds){
            if(bind instanceof Picker){
                Picker picker = (Picker) bind;
                String tag = picker.provideFragmentTag();
                PickerFragment fragment = (PickerFragment) manager.findFragmentByTag(tag);

                if(fragment == null){
                    continue;
                }

                fragment.setBind(bind);
            }
        }
    }

    private void restorePersistent(FormBind bind, MaizActivity activity) {
        final PersistentMemento memento = (PersistentMemento) bind;
        final String value = Persistent.with(activity,memento.provideFile()).read(memento.provideKey());
        bind.write(value);
    }

    private void restoreBundle(@NonNull FormBind bind, @Nullable Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return;
        }

        final BundleMemento memento = (BundleMemento) bind;
        final String value = savedInstanceState.getString(memento.provideKey());
        bind.write(value);
    }

    @Override
    public void atStop(MaizActivity activity) {
        final ArrayList<FormBind> binds = provideBinds();

        for(FormBind bind : binds){
            if(bind instanceof PersistentMemento){
                savePersistent(bind,activity);
            }
        }
    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {
        final ArrayList<FormBind> binds = provideBinds();

        for(FormBind bind : binds){
            if(bind instanceof BundleMemento){
                saveBundle(bind,outstate);
            }
        }
    }

    private void saveBundle(FormBind bind, Bundle outstate) {
        final BundleMemento memento = (BundleMemento) bind;
        outstate.putString(memento.provideKey(),bind.read());
    }

    private void savePersistent(FormBind bind, MaizActivity activity) {
        final PersistentMemento memento = (PersistentMemento) bind;
        Persistent.with(activity,memento.provideFile()).write(memento.provideKey(),bind.read());
    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }

    @Nullable
    public FormBind resolvBind(String fieldName){
        final ArrayList<FormBind> binds = provideBinds();
        final int index = fieldLookup.get(fieldName);

        if(binds.size() > index){
            return binds.get(index);
        }

        return null;
    }

    public HashMap<String,Entity> resolvEntities() {
        final HashMap<String,Entity> data = new HashMap<>();
        final ArrayList<FormBind> binds = provideBinds();

        for(FormBind bind : binds){
            String value = bind.read();

            if(value == null){
                continue;
            }

            dataBind(data,bind,value);
        }

        return data;
    }

    private void dataBind(HashMap<String, Entity> data, FormBind bind, String value) {
        if(bind instanceof FileBind){
            fileBind(data,(FileBind) bind,value);
        }
        else {
            data.put(bind.getFieldName(), new Entity(value.getBytes()));
        }
    }

    private void fileBind(HashMap<String, Entity> data, FileBind bind, String file) {
        final byte[] array = FileReader.bufferDump(file);

        if(array == null){
            return;
        }

        final String mime = bind.provideMime();

        if(mime == null){
            return;
        }

        data.put(bind.getFieldName(), new FileEntity(array,mime));
    }
}
