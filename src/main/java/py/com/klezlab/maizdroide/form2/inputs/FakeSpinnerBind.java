package py.com.klezlab.maizdroide.form2.inputs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.form2.FormBind;
import py.com.klezlab.maizdroide.form2.rules.AbstractRule;

public abstract class FakeSpinnerBind extends FormBind {
    private final Spinner spinner;
    private final TextView label;
    private final View trigger;

    private ArrayList<AbstractRule> rules;
    private String value;

    public FakeSpinnerBind(@NonNull FakeSpinnerManifesto manifesto) {
        super(manifesto.spinner, manifesto.fieldName, manifesto.validation);

        spinner = manifesto.spinner;
        label = manifesto.label;
        trigger = manifesto.trigger;

        trigger.setClickable(true);
        spinner.setSelection(resolv(manifesto.defaultValue));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                atItemSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

        trigger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicker();
            }
        });
    }

    @NonNull
    @Override
    public ArrayList<AbstractRule> provideRules() {
        if(rules == null){
            rules = new ArrayList<>();
            rules.add(new AbstractRule() {
                @Override
                public String provideValidationMessage() {
                    return FakeSpinnerBind.this.provideValidationMessage();
                }

                @Override
                public boolean validates(@Nullable String value) {
                    return FakeSpinnerBind.this.validates(value);
                }
            });
        }

        return rules;
    }

    @Override
    public void write(@Nullable String value) {
        final int index = resolv(value);

        if(index > 0){
            this.value = value;
            this.spinner.setSelection(index);
        }
    }

    private void atItemSelected(int position) {
        final String lbl = resolvLabel(position);
        label.setText(lbl);

        this.value = resolvValue(position);
    }

    private void clicker() {
        spinner.performClick();
    }

    @Nullable
    @Override
    public String read() {
        return value;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    protected abstract int resolv(String value);
    protected abstract String resolvValue(int position);
    protected abstract String resolvLabel(int position);
    protected abstract String provideValidationMessage();
    protected abstract boolean validates(@Nullable String value);
}
