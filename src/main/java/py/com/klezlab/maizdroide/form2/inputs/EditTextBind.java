package py.com.klezlab.maizdroide.form2.inputs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import py.com.klezlab.maizdroide.form2.FormBind;

public abstract class EditTextBind extends FormBind {
    private final EditText input;

    public EditTextBind(@NonNull EditText input,@NonNull String fieldName, @Nullable TextView validation) {
        super(input, fieldName, validation);

        this.input = input;
    }

    @Override
    public void write(@Nullable String value) {
        input.setText(value);
    }

    @Nullable
    @Override
    public String read() {
        return input.getText().toString();
    }
}
