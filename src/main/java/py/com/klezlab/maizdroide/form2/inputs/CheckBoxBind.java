package py.com.klezlab.maizdroide.form2.inputs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.CheckBox;

import py.com.klezlab.maizdroide.form2.FormBind;

public abstract class CheckBoxBind extends FormBind{
    private final String positiveValue;
    private final String negativeValue;
    private final CheckBox checkBox;

    public CheckBoxBind(@NonNull CheckBoxManifesto manifesto) {
        super(manifesto.input, manifesto.fieldName, manifesto.validation);

        this.checkBox = manifesto.input;
        this.positiveValue = manifesto.positiveValue;
        this.negativeValue = manifesto.negativeValue;
    }

    @Nullable
    @Override
    public String read() {
        if(checkBox.isChecked()){
            return positiveValue;
        }
        else{
            return negativeValue;
        }
    }


    public void write(boolean b) {
        if(b){
            write(positiveValue);
        }
        else{
            write(negativeValue);
        }
    }

    @Override
    public void write(@Nullable String value) {
        if(value == null){
            return;
        }

        checkBox.setChecked(value.equals(positiveValue));
    }
}
