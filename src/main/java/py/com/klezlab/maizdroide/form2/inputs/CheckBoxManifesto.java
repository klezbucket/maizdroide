package py.com.klezlab.maizdroide.form2.inputs;

import android.widget.CheckBox;
import android.widget.TextView;

public class CheckBoxManifesto {
    public String positiveValue;
    public String negativeValue;
    public String fieldName;
    public CheckBox input;
    public TextView validation;
}
