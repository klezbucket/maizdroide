package py.com.klezlab.maizdroide.form2.inputs;

import android.support.v4.app.FragmentManager;
import android.widget.TextView;

public class FacadeDatePickerManifesto {
    public String format;
    public String tag;
    public FragmentManager fragmentManager;
    public TextView facade;
    public String fieldName;
    public TextView validation;
}
