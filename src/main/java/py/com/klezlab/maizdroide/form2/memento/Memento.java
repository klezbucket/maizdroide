package py.com.klezlab.maizdroide.form2.memento;

public interface Memento {
    String provideKey();
}
