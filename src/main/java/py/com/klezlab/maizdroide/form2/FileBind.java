package py.com.klezlab.maizdroide.form2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

public abstract class FileBind extends FormBind {
    public FileBind(@NonNull View input, @NonNull String fieldName, @Nullable TextView validation) {
        super(input,fieldName,validation);
    }

    public abstract String provideMime();
}
