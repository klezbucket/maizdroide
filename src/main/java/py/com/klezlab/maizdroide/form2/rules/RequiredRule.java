package py.com.klezlab.maizdroide.form2.rules;

import android.support.annotation.Nullable;

public abstract class RequiredRule extends AbstractRule {
    @Override
    public boolean validates(@Nullable String value) {
        if(value == null){
            return false;
        }

        if(value.trim().equals("")){
            return false;
        }

        return true;
    }
}
