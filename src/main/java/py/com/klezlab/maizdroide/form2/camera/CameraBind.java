package py.com.klezlab.maizdroide.form2.camera;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.camera.CameraAbstractEngine;
import py.com.klezlab.maizdroide.form2.FileBind;

public abstract class CameraBind extends FileBind {
    private final CameraAbstractEngine engine;

    public CameraBind(@NonNull CameraManifesto manifesto) {
        super(manifesto.input, manifesto.fieldName, manifesto.validation);

        this.engine = manifesto.engine;
    }

    @Nullable
    @Override
    public String read() {
        if(engine.hasFile() == false){
            return null;
        }

        return engine.providePath();
    }

    @Override
    public void write(@Nullable String value) {

    }

    @Override
    public String provideMime() {
        return engine.provideMime();
    }
}
