package py.com.klezlab.maizdroide.form2;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.Hook;

public interface FormHook extends Hook {
    @NonNull ArrayList<FormBind> provideFormBinds();
}
