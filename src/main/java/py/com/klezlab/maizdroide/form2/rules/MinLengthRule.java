package py.com.klezlab.maizdroide.form2.rules;

import android.support.annotation.Nullable;

public abstract class MinLengthRule extends AbstractRule {
    @Override
    public boolean validates(@Nullable String value) {
        if(value == null){
            return false;
        }

        final int length = value.trim().length();
        return length >= provideMinLength();
    }

    protected abstract int provideMinLength();
}
