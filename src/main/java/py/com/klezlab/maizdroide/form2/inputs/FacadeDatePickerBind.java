package py.com.klezlab.maizdroide.form2.inputs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.form2.FormBind;
import py.com.klezlab.maizdroide.form2.fragment.PickerFragment;

public abstract class FacadeDatePickerBind extends FormBind implements Picker {
    private static final String INTERNAL_FORMAT = "yyyy-MM-dd";
    private static final long UNSET_VALUE = -1;
    private final TextView facade;
    private long timestamp;
    private String format;
    private FragmentManager fragmentManager;
    private String tag;
    private DatePickerFragment fragment;

    public FacadeDatePickerBind(@NonNull FacadeDatePickerManifesto manifesto) {
        super(manifesto.facade,manifesto.fieldName,manifesto.validation);

        this.facade = manifesto.facade;
        this.format = manifesto.format;
        this.tag = manifesto.tag;
        this.fragmentManager = manifesto.fragmentManager;

        facade.setClickable(true);
        facade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facadeClicker();
            }
        });
    }

    @Override
    public String provideFragmentTag() {
        return tag;
    }

    private void facadeClicker() {
        if(fragment != null) {
            fragment.dismiss();
        }

        fragment = new DatePickerFragment();
        fragment.setBind(this);
        fragment.show(fragmentManager, tag);
    }

    @Nullable
    @Override
    public String read() {
        return resolvDate(INTERNAL_FORMAT);
    }

    @Override
    public void write(@Nullable String value) {
        try {
            timestamp = resolvTimestamp(value);
        }
        catch (ParseException e) {
            Logcat.exception("ParseException @ FacadeDatePicker::write(" + value + ")",e);
            timestamp = UNSET_VALUE;
        }

        facade.setText(resolvDate(format));
    }

    private long resolvTimestamp(@Nullable String value) throws ParseException {
        if(value == null || "".equals(value)){
            return UNSET_VALUE;
        }

        final SimpleDateFormat formatter = new SimpleDateFormat(INTERNAL_FORMAT);
        final Date date = formatter.parse(value);
        return date.getTime();
    }

    private String resolvDate(String format){
        if (timestamp == UNSET_VALUE) {
            return facade.getText().toString();
        }

        final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        final String formatDate  = dateFormat.format(new Date(timestamp));
        return formatDate;
    }

    public static class DatePickerFragment extends PickerFragment {
        private static final String DASH = "-";

        public DatePickerFragment() {
            super();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final FacadeDatePickerBind bind = (FacadeDatePickerBind) getBind();
            final Calendar calendar = setupCalendar(bind.timestamp);

            final int year = calendar.get(Calendar.YEAR);
            final int month = calendar.get(Calendar.MONTH);
            final int day = calendar.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        private Calendar setupCalendar(long timestamp) {
            final Calendar calendar =  Calendar.getInstance();

            if(timestamp == UNSET_VALUE){
                return calendar;
            }

            final Date date = new Date();
            date.setTime(timestamp);
            calendar.setTime(date);

            return calendar;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            final String date = resolvDate(year,month,day);
            final FacadeDatePickerBind bind = (FacadeDatePickerBind) getBind();

            if(bind == null){
                return;
            }

            bind.write(date);
        }

        private String resolvDate(int year,int month,int day){
            final StringBuilder sb = new StringBuilder()
                    .append(String.format("%04d",year))
                    .append(DASH)
                    .append(String.format("%02d",month + 1))
                    .append(DASH)
                    .append(String.format("%02d",day));

            return sb.toString();
        }

    }
}
