package py.com.klezlab.maizdroide.form2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.form2.rules.AbstractRule;

public abstract class FormBind {
    private final TextView validation;
    private final View input;
    private final Context context;
    private final String fieldName;
    private String validationMessage;
    private boolean validationStatus;

    public FormBind(@NonNull View input, @NonNull String fieldName, @Nullable TextView validation) {
        this.context = input.getContext();
        this.validation = validation;
        this.input = input;
        this.fieldName = fieldName;
    }

    public void hideValidation(){
        if(validation == null){
            return;
        }
        validation.setVisibility(View.GONE);
        validation.setText("");
    }

    public void flushValidation(){
        if(validation == null){
            return;
        }

        hideValidation();
    }

    public void showValidation(){
        if(validation == null){
            return;
        }

        validation.setVisibility(View.VISIBLE);
        validation.setText(validationMessage);
    }

    public void toggleValidation(){
        if(validationStatus){
            flushValidation();
        }
        else{
            showValidation();
        }
    }

    public Context getContext() {
        return context;
    }

    public void invalidate(String message) {
        validationMessage = message;
        validationStatus = false;
    }

    public void validate(){
        validationMessage = null;
        validationStatus = true;
    }

    public View getInput() {
        return input;
    }

    public String getFieldName() {
        return fieldName;
    }

    @NonNull public abstract ArrayList<AbstractRule> provideRules();
    @Nullable public abstract String read();
    public abstract void write(@Nullable String value);
}
