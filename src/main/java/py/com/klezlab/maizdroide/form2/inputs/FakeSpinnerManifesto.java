package py.com.klezlab.maizdroide.form2.inputs;

import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class FakeSpinnerManifesto {
    public Spinner spinner;
    public View trigger;
    public TextView label;
    public String fieldName;
    public TextView validation;
    public String defaultValue;
}
