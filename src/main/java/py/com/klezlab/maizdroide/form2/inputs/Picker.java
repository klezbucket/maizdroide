package py.com.klezlab.maizdroide.form2.inputs;

public interface Picker {
    String provideFragmentTag();
}
