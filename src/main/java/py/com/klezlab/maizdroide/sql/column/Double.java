package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class Double extends SqliteColumn {
    public Double(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.DOUBLE;
    }
}
