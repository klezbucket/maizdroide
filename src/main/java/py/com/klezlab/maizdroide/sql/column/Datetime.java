package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class Datetime extends SqliteColumn {

    public Datetime(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.DATETIME;
    }
}
