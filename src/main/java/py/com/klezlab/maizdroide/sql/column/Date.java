package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class Date extends SqliteColumn {

    public Date(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.DATE;
    }
}
