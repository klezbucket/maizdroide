package py.com.klezlab.maizdroide.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.sql.column.Index;
import py.com.klezlab.maizdroide.strings.Strings;

abstract public class Sqlite extends SQLiteOpenHelper {
    public static final String TEXT = "TEXT";
    public static final String INTEGER = "INTEGER";
    public static final String BIG_INTEGER = "BIGINT";
    public static final String DOUBLE = "DOUBLE";
    public static final String DATE = "DATE";
    public static final String DATETIME = "DATETIME";

    private Context context;
    private VersionHandler version;
    private SqliteSchema schema;
    private String table;
    private HashMap<String,Integer> mappings = new HashMap<>();

    public String getTable() {
        return table;
    }

    public Sqlite(VersionHandler version, Context context, SqliteSchema schema) {
        super(context, version.getName(), null, version.getVersion());

        this.context = context;
        this.version = version;
        this.schema = schema;
        this.table = provideTableName();

        mappings();
    }

    public void truncate(SQLiteDatabase database){
        database.delete(table,null,null);
    }

    public void truncate(){
        SQLiteDatabase database = this.getWritableDatabase();
        truncate(database);
        database.close();
    }

    private void mappings() {
        Iterator iterator = schema.getIterator();
        int j = 0;

        while(iterator.hasNext()){
            SqliteColumn column = (SqliteColumn) iterator.next();
            mappings.put(column.getName(),j);
            j++;
        }
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }

    public void write(String colName, String value){
        if(mappings.containsKey(colName)) {
            int j = mappings.get(colName);
            SqliteColumn col = schema.get(j);
            col.setValue(value);
        }
    }

    public String read(String colName){
        if(mappings.containsKey(colName)) {
            int j = mappings.get(colName);
            SqliteColumn col = schema.get(j);
            return col.getValue();
        }

        return null;
    }

    public abstract String provideTableName();
    public abstract void upgradeTable(SQLiteDatabase db, int version);

    protected VersionHandler getVersion(){
        return version;
    }

    protected Context getContext(){
        return context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        version.onCreate(db);
//        version.onUpgrade(db, 1, version.getVersion());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        version.onUpgrade(db, oldVersion, newVersion);
    }

    public void createTable(SQLiteDatabase db){
        String sql = getCreateTableSql();
        db.beginTransaction();
        db.execSQL(sql);
        createIndices(db);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    private void createIndices(SQLiteDatabase db) {
        Iterator iterator = schema.getIterator();

        while (iterator.hasNext()){
            SqliteColumn col = (SqliteColumn) iterator.next();

            if(col instanceof Index){
                String sql = getCreateIndexSql(col);
                db.execSQL(sql);
            }
        }
    }

    protected String getCreateIndexSql(SqliteColumn col) {
        StringBuilder sb = new StringBuilder();
        Index index = (Index) col;

        sb.append("CREATE INDEX IF NOT EXISTS ");
        sb.append(index.provideIndexName(getTable()));
        sb.append(" ON ");
        sb.append(getTable());
        sb.append("(");
        sb.append(col.getName());
        sb.append(")");

        return sb.toString();
    }

    protected String getAddColumnSql(SqliteColumn col){
        StringBuilder sb = new StringBuilder();
        sb.append("ALTER TABLE ");
        sb.append(provideTableName());
        sb.append(" ADD COLUMN ");
        sb.append(col.getName());
        sb.append(" ");
        sb.append(col.provideColumnType());
        sb.append(" ");
        sb.append(SqliteColumn.getColumnExtras(col));

        return sb.toString();
    }

    public void batchLoad(ArrayList<Map> collection, SQLiteDatabase database) {
        truncate(database);

        for(Map row : collection){
            write(providePrimaryField(),null);

            final Iterator<SqliteColumn> iterator = getSchema().getIterator();

            while(iterator.hasNext()){
                String field = iterator.next().getName();
                String value = Strings.extractor(row.get(field));
                write(field, value);
            }

            insert(database);
        }
    }

    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        Iterator iterator = schema.getIterator();

        while (iterator.hasNext()){
            SqliteColumn col = (SqliteColumn) iterator.next();
            values.put(col.getName(),col.getValue());
        }

        return values;
    }

    public SqliteSchema getSchema() {
        return schema;
    }

    public String getCreateTableSql(){
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ");
        sb.append(table);
        sb.append(" ( ");

        Iterator it = schema.getIterator();

        while (it.hasNext()) {
            SqliteColumn column = (SqliteColumn) it.next();
            sb.append(column.getName());
            sb.append(" ");
            sb.append(column.provideColumnType());
            sb.append(" ");
            sb.append(SqliteColumn.getColumnExtras(column));

            if(it.hasNext()) {
                sb.append(",");
            }
        }

        sb.append(" )");
        return sb.toString();
    }

    protected String[] getFields(){
        String[] fields = new String[schema.size()];
        int i = 0;

        Iterator it = schema.getIterator();

        while (it.hasNext()){
            SqliteColumn column = (SqliteColumn) it.next();
            String field = column.getName();
            fields[i] = field;
            i += 1;
        }

        return fields;
    }

    public void upsert(SQLiteDatabase db) {
        if(primaryExists(db,providePrimaryField())){
            update(db);
        }
        else {
            insert(db);
        }
    }

    public void upsert() {
        if(primaryExists(providePrimaryField())){
            update();
        }
        else {
            insert();
        }
    }

    public void update(SQLiteDatabase database) {
        String primary = providePrimaryField();
        StringBuilder where = new StringBuilder();
        where.append(primary);
        where.append("= ?");

        database.update(getTable(), getContentValues(), where.toString(), new String[]{read(primary)});
    }

    public void update() {
        SQLiteDatabase database = this.getWritableDatabase();
        update(database);
        database.close();
    }

    protected boolean primaryExists(SQLiteDatabase database,String primary) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ");
        sql.append(getTable());
        sql.append(" WHERE ");
        sql.append(primary);
        sql.append(" = ?");

        String id = read(primary);

        if(id == null){
            return false;
        }

        Cursor cursor = database.rawQuery(sql.toString(),new String[] { id });
        boolean exists = cursor.getCount() > 0;
        cursor.close();

        return exists;
    }

    protected boolean primaryExists(String primary) {
        SQLiteDatabase database = this.getWritableDatabase();
        boolean exists = primaryExists(database,primary);
        database.close();

        return exists;
    }

    protected abstract String providePrimaryField();

    public void insert() {
        SQLiteDatabase database = this.getWritableDatabase();
        insert(database);
        database.close();
    }

    public void insert(SQLiteDatabase database) {
        database.insert(getTable(), null, getContentValues());
    }

    public Leakable find(SQLiteDatabase database,Query query){
        Cursor cursor = database.query(
                table,
                query.select,
                query.where,
                query.whereArgs,
                query.groupBy,
                query.having,
                query.order,
                query.limit
        );

        Leakable leakable = new Leakable(database,cursor);
        return leakable;
    }

    public Leakable find(Query query){
        SQLiteDatabase database = this.getWritableDatabase();
        return find(database,query);
    }

    public void load(Cursor cursor){
        Iterator iterator = schema.getIterator();

        while(iterator.hasNext()){
            SqliteColumn col = (SqliteColumn) iterator.next();
            String value = getCursorString(col,cursor);
            write(col.getName(),value);
        }
    }

    private String getCursorString(SqliteColumn col,Cursor cursor) {
        String name = col.getName();
        int index = cursor.getColumnIndex(name);

        if(TEXT.equals(col.provideColumnType())){
            return cursor.getString(index);
        }

        if(DOUBLE.equals(col.provideColumnType())){
            return String.valueOf(cursor.getDouble(index));
        }

        if(INTEGER.equals(col.provideColumnType())){
            return String.valueOf(cursor.getInt(index));
        }

        if(BIG_INTEGER.equals(col.provideColumnType())){
            return String.valueOf(cursor.getLong(index));
        }

        if(DATE.equals(col.provideColumnType())){
            return cursor.getString(index);
        }

        if(DATETIME.equals(col.provideColumnType())){
            return cursor.getString(index);
        }

        return null;
    }
    public Leakable findAll(SQLiteDatabase database) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ");
        sql.append(getTable());

        Cursor cursor = database.rawQuery(sql.toString(), null);
        Leakable leakable = new Leakable(database,cursor);
        return leakable;
    }

    public Leakable findAll() {
        SQLiteDatabase database = this.getWritableDatabase();
        return findAll(database);
    }

    public boolean findById(SQLiteDatabase database,String id) {
        StringBuilder sb = new StringBuilder()
                .append(providePrimaryField())
                .append(" = ?");

        Query query = new Query();
        query.setLimit(1, 0);
        query.setWhere(sb.toString());
        query.setWhereArgs(new String[]{id});

        Leakable leakable;

        if(database != null) {
            leakable = find(database, query);
        }
        else{
            leakable = find(query);
        }

        Cursor cursor = leakable.getCursor();

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            load(cursor);
            closeTheLeakable(database,leakable);

            return true;
        }

        closeTheLeakable(database,leakable);
        return false;
    }

    protected void closeTheLeakable(SQLiteDatabase database,Leakable leakable){
        if(database == null){
            leakable.close();
        }
        else{
            leakable.closeCursor();
        }
    }

    public boolean findById(long id) {
        return findById(null, String.valueOf(id));
    }

    public boolean findById(String id) {
        return findById(null,id);
    }

    public void clone(Sqlite sqlite) {
        this.schema = sqlite.schema;
    }

    public Leakable batchTest(SQLiteDatabase database) {
        final Leakable leakable = findAll(database);
        final Cursor cursor = leakable.getCursor();

        while(cursor.moveToNext()){
            load(cursor);
            Logcat.debug(toString());
        }

        return leakable;
    }

    public void batchTest() {
        final SQLiteDatabase database = this.getWritableDatabase();
        final Leakable leakable = batchTest(database);
        leakable.close();
    }

    public boolean findFirst(){
        SQLiteDatabase database = this.getWritableDatabase();
        return findFirst(database);

    }

    public boolean findFirst(SQLiteDatabase database) {
        final StringBuilder order = new StringBuilder()
                .append(providePrimaryField())
                .append(" DESC");

        Query query = new Query();
        query.setLimit(1, 0);
        query.setOrder(order.toString());

        Leakable leakable;

        if(database != null) {
            leakable = find(database, query);
        }
        else{
            leakable = find(query);
        }

        Cursor cursor = leakable.getCursor();

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            load(cursor);
            closeTheLeakable(database,leakable);

            return true;
        }

        closeTheLeakable(database,leakable);
        return false;
    }

    public static class Query{
        private String[] select;
        private String where;
        private String[] whereArgs;
        private String groupBy;
        private String having;
        private String limit;
        private String order;

        public void setWhere(String where) {
            this.where = where;
        }

        public void setWhereArgs(String[] whereArgs) {
            this.whereArgs = whereArgs;
        }

        public void setLimit(int limit,int offset) {
            StringBuilder sb = new StringBuilder();
            sb.append(offset);
            sb.append(", ");
            sb.append(limit);

            this.limit = sb.toString();
        }

        public void setOrder(String order) {
            this.order = order;
        }

        public void setGroupBy(String groupBy) {
            this.groupBy = groupBy;
        }

        public void setHaving(String having) {
            this.having = having;
        }

        public void setSelect(String[] select) {
            this.select = select;
        }

        public String getWhere() {
            return where;
        }

        public String[] getWhereArgs() {
            return whereArgs;
        }

        @Override
        public String toString() {
            return "Query{" +
                    "select=" + Arrays.toString(select) +
                    ", where='" + where + '\'' +
                    ", whereArgs=" + Arrays.toString(whereArgs) +
                    ", groupBy='" + groupBy + '\'' +
                    ", having='" + having + '\'' +
                    ", limit='" + limit + '\'' +
                    ", order='" + order + '\'' +
                    '}';
        }
    }

    public void delete(long id) {
        delete(String.valueOf(id));
    }

    public void delete(String id) {
        SQLiteDatabase database = this.getWritableDatabase();
        StringBuilder where = new StringBuilder()
                .append(providePrimaryField())
                .append(" = ?");

        String[] whereArgs = new String[] { id };
        database.delete(table,where.toString(),whereArgs);
        database.close();
    }

    public class Leakable {
        private SQLiteDatabase sqLiteDatabase;
        private Cursor cursor;

        public void close(){
            closeCursor();

            if(sqLiteDatabase != null) {
                sqLiteDatabase.close();
            }
        }

        public void closeCursor(){
            if(cursor != null) {
                cursor.close();
            }
        }

        public Leakable(SQLiteDatabase sqLiteDatabase, Cursor cursor) {
            this.sqLiteDatabase = sqLiteDatabase;
            this.cursor = cursor;
        }

        public SQLiteDatabase getSqLiteDatabase() {
            return sqLiteDatabase;
        }

        public Cursor getCursor() {
            return cursor;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String c = this.getClass().getSimpleName();
        sb.append(c);
        sb.append(" |");

        Iterator iterator = getSchema().getIterator();

        while (iterator.hasNext()) {
            SqliteColumn col = (SqliteColumn) iterator.next();

            sb.append(" ");
            sb.append(col.getName());
            sb.append("<");
            sb.append(col.getValue());
            sb.append(">");
        }

        return sb.toString();
    }
}