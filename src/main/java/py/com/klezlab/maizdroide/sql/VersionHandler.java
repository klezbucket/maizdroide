package py.com.klezlab.maizdroide.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

abstract public class VersionHandler {
    private final ArrayList<Sqlite> models;
    private int version = 1;
    private String name = "database_name";

    public VersionHandler(Context ctx,String name, int version){
        this.version = version;
        this.name = name;
        this.models = provideModels(ctx);
    }

    public int getVersion() {
        return version;
    }

    public String getName() {
        return name;
    }

    public void onCreate(SQLiteDatabase db) {
        for(Sqlite model : models){
            model.createTable(db);
        }
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for(Sqlite model : models){
            for(int i = oldVersion;i <= newVersion;i++) {
                model.upgradeTable(db,i);
            }
        }
    }

    abstract public ArrayList<Sqlite> provideModels(Context ctx);
}