package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class BigInteger extends SqliteColumn {
    public BigInteger(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.BIG_INTEGER;
    }
}
