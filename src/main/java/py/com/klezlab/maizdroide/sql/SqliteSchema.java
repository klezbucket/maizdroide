package py.com.klezlab.maizdroide.sql;

import java.util.ArrayList;
import java.util.Iterator;

public class SqliteSchema {
    private ArrayList<SqliteColumn> schema = new ArrayList<>();

    public void add(SqliteColumn col){
        schema.add(col);
    }

    public int size(){
        return schema.size();
    }

    public Iterator<SqliteColumn> getIterator(){
        return schema.iterator();
    }

    public SqliteColumn get(int index){
        return schema.get(index);
    }
}
