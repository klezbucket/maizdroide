package py.com.klezlab.maizdroide.sql;

import py.com.klezlab.maizdroide.sql.column.Integer;
import py.com.klezlab.maizdroide.sql.column.Primary;

abstract public class SqliteColumn {
    private static final String PRIMARY_EXTRA_DEF = "PRIMARY KEY";
    private static final String AUTO_INCREMENT_EXTRA_DEF = "AUTOINCREMENT";
    private String name;
    private String value;

    public SqliteColumn(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    abstract public String provideColumnType();

    public static String getColumnExtras(SqliteColumn column) {
        if(column instanceof Primary){
            return getPrimaryExtras(column);
        }
        return "";
    }

    private static String getPrimaryExtras(SqliteColumn column) {
        StringBuilder sb = new StringBuilder();
        sb.append(PRIMARY_EXTRA_DEF);
        sb.append(" ");

        if(column instanceof Integer){
            sb.append(AUTO_INCREMENT_EXTRA_DEF);
        }

        return sb.toString();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
