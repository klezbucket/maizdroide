package py.com.klezlab.maizdroide.sql.index;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;
import py.com.klezlab.maizdroide.sql.column.Index;

public class DateIndex extends SqliteColumn implements Index {
    public DateIndex(String name) {
        super(name);
    }

    @Override
    public String provideIndexName(String table) {
        final StringBuilder sb = new StringBuilder()
                .append(table)
                .append(Index.SEPARATOR)
                .append(getName());

        return sb.toString();
    }

    @Override
    public String provideColumnType() {
        return Sqlite.DATE;
    }
}
