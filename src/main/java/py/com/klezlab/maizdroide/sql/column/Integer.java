package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

/**
 * Created by klez on 06/11/15.
 */
public class Integer extends SqliteColumn {
    public Integer(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.INTEGER;
    }
}
