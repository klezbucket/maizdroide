package py.com.klezlab.maizdroide.sql.column;

public interface Index {
    String SEPARATOR = "_indexof_";
    String provideIndexName(String table);
}
