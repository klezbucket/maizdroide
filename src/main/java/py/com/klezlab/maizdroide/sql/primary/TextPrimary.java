package py.com.klezlab.maizdroide.sql.primary;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;
import py.com.klezlab.maizdroide.sql.column.Primary;

public class TextPrimary extends SqliteColumn implements Primary {
    public TextPrimary(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.TEXT;
    }
}