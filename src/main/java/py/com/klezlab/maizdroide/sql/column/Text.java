package py.com.klezlab.maizdroide.sql.column;


import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class Text extends SqliteColumn {
    public Text(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.TEXT;
    }
}
