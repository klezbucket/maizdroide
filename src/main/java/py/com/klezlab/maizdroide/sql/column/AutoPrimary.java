package py.com.klezlab.maizdroide.sql.column;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteColumn;

public class AutoPrimary extends SqliteColumn implements Primary{
    public AutoPrimary(String name) {
        super(name);
    }

    @Override
    public String provideColumnType() {
        return Sqlite.INTEGER;
    }
}
