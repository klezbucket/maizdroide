package py.com.klezlab.maizdroide.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Stack;

import py.com.klezlab.maizdroide.activity.BackPressedHandler;
import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.debug.Logcat;

public abstract class FragmentAbstractEngine implements FragmentHook, BackPressedHandler {
    private static final String CURRENT_FRAGMENT = "py.com.v.maizdroide.fragment.CURRENT_FRAGMENT";
    private static final String HISTORY_STACK = "py.com.klezlab.maizdroide.fragment.HISTORY_STACK";
    private Stack<String> history;
    private BaseFragment fragment;

    @Override
    public boolean atBackPressed(MaizActivity activity) {
        Logcat.debug("BACK PRESSED");

        if(history.empty()){
            atEmptyHistory();
        }
        else{
            history.pop();

            if(history.empty() == false) {
                final String tag = history.peek();
                final BaseFragment frag = getFragmentByTag(activity,tag);
                changeFragment(activity,frag);
            }
            else{
                atBackPressed(activity);
            }
        }

        return true;
    }

    private void debugStack() {
        for(String tag : history){
            Logcat.debug("tag:"+tag);
        }

        Logcat.debug("tag:##################################################");
    }

    private void addHistoryStack(String tag) {
        if(history == null){
            history = new Stack<>();
        }

        if( ! history.empty()) {
            final String top = history.peek();

            if(tag.equals(top)){
                return;
            }
        }

        history.push(tag);
    }

    @Nullable
    private BaseFragment getFragmentByTag(MaizActivity activity,String tag){
        final BaseFragment frag = (BaseFragment) activity.getSupportFragmentManager().findFragmentByTag(tag);
        return frag;
    }

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        BaseFragment frag = null;

        if(savedInstanceState != null){
            restoreHistory(savedInstanceState);
            restoreFragmentState(activity,savedInstanceState);

            final String tag = savedInstanceState.getString(CURRENT_FRAGMENT);
            frag = (BaseFragment) activity.getSupportFragmentManager().findFragmentByTag(tag);

            if(history.isEmpty() == false) {
                history.pop();
            }
        }

        if (frag == null) {
            frag = provideInitialFragment();
        }

        changeFragment(activity,frag);
    }

    private void restoreFragmentState(MaizActivity activity, Bundle savedInstanceState) {
        FragmentTransaction tx = activity.getSupportFragmentManager().beginTransaction();
        int i = 0;

        for (BaseFragment frag : provideFragments()) {
            String tag = frag.provideFragmentTag();
            BaseFragment base = (BaseFragment) activity.getSupportFragmentManager().getFragment(savedInstanceState,tag);

            if(base != null){
                provideFragments().set(i,base);
                tx.hide(base);
            }

            i++;
        }

        tx.commit();
        activity.getSupportFragmentManager().executePendingTransactions();
    }

    public void changeFragment(MaizActivity activity, BaseFragment frag){
        if(frag.equals(fragment)) {
            return;
        }

        final FragmentManager manager = activity.getSupportFragmentManager();
        final FragmentTransaction transaction = manager.beginTransaction();

        if (fragment != null && fragment.isHidden() == false) {
            transaction.hide(fragment);
        }

        if (frag.isAdded() == false) {
            transaction.add(provideFrame(), frag, frag.provideFragmentTag());
        }

        if (frag.isHidden()) {
            transaction.show(frag);
        }

        if (frag.isHistoryStacked()) {
//            transaction.addToBackStack(frag.provideFragmentTag());
            addHistoryStack(frag.getTag());
        }

        fragment = frag;

        transaction.commit();
        manager.executePendingTransactions();
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity,Bundle outstate) {
        for(BaseFragment frag : provideFragments()){
            String tag = frag.provideFragmentTag();

            if(frag.isAdded()) {
                activity.getSupportFragmentManager().putFragment(outstate, tag, frag);
            }
        }

        if(fragment != null) {
            outstate.putString(CURRENT_FRAGMENT, fragment.provideFragmentTag());
        }

        saveHistoryStack(outstate);
    }

    private void saveHistoryStack(Bundle outstate) {
        if(history == null){
            return;
        }

        final String[] tags = new String[history.size()];
        int i = 0;

        for(String tag : history){
            tags[i] = tag;
            i++;
        }

        outstate.putStringArray(HISTORY_STACK,tags);
    }

    private void restoreHistory(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return;
        }

        final String[] tags = savedInstanceState.getStringArray(HISTORY_STACK);
        history = new Stack<>();

        if(tags == null){
            return;
        }

        for(String tag : tags){
            history.push(tag);
        }
    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    protected abstract void atEmptyHistory();


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
