package py.com.klezlab.maizdroide.fragment;

import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.adapter.BaseAdapter;
import py.com.klezlab.maizdroide.thread.AsyncThread;

public abstract class ScrollAbstractFragment extends BaseFragment {
    private BaseAdapter adapter;
    private RecyclerView recycler;
    private LinearLayoutManager layoutManager;

    public RecyclerView getRecycler() {
        return recycler;
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    @Override
    protected void atCreateView(View root) {
        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recycler = (RecyclerView) root.findViewById(R.id.recycler);
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(layoutManager);
        recycler.setItemViewCacheSize(20);
        recycler.setDrawingCacheEnabled(true);
        recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        adapter = provideAdapter();

        if(adapter.isForeverScrolled()){
            recycler.addOnScrollListener(new ForeverScroller());
        }

        recycler.setAdapter(adapter);
    }

    @Override
    protected int provideLayout() {
        return R.layout.scroll_abstract_fragment;
    }

    private class ForeverScroller extends RecyclerView.OnScrollListener {
        private AsyncThread fetcher;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if(fetcher != null && fetcher.isFinished() == false){
                return;
            }

            if (dy > 0) {
                int visibleItemCount = getLayoutManager().getChildCount();
                int totalItemCount = adapter.getItemCount();
                int pastVisiblesItems = getLayoutManager().findFirstVisibleItemPosition();

                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    waiting();
                    asyncFetch();
                }
            }
        }

        private void waiting() {
            if(adapter instanceof ForeverScrollerAdapter){
                final ForeverScrollerAdapter scrollerAdapter = (ForeverScrollerAdapter) adapter;
                scrollerAdapter.atWaiting();
            }
        }

        private void asyncFetch() {
            fetcher = new AsyncFetcher();
            fetcher.run();
        }
    }

    private class AsyncFetcher extends AsyncThread{
        private static final long FETCH_DELAY = 1000;

        public AsyncFetcher() {
            super(FETCH_DELAY);
        }

        @Override
        protected void afterTask(@Nullable Object obj) {
            if(adapter instanceof ForeverScrollerAdapter){
                final ForeverScrollerAdapter scrollerAdapter = (ForeverScrollerAdapter) adapter;
                scrollerAdapter.atDataUpdate(obj);
            }
        }

        @Nullable
        @Override
        protected Object task(@Nullable Object[] params) {
            if(adapter instanceof ForeverScrollerAdapter){
                final ForeverScrollerAdapter scrollerAdapter = (ForeverScrollerAdapter) adapter;
                return scrollerAdapter.atFetch(getContext());
            }

            return null;
        }

        @Nullable
        @Override
        protected Object beforeTask() {
            return null;
        }
    }

    protected abstract BaseAdapter provideAdapter();
}
