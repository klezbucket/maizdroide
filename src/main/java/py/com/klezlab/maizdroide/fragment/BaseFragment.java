package py.com.klezlab.maizdroide.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

abstract public class BaseFragment extends Fragment {
    protected abstract void atCreateView(View root);
    protected abstract void atRestoreInstaceState(@Nullable Bundle savedInstanceState);
    protected abstract void atSaveInstanceState(Bundle outState);
    public abstract String provideFragmentTag();
    protected abstract int provideLayout();
    public abstract boolean isHistoryStacked();
    public abstract void atShow();
    public abstract void atHide();

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if(isResumed() == false){
            return;
        }

        if(hidden){
            atHide();
        }
        else{
            atShow();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        atSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View root = inflater.inflate(provideLayout(), container, false);
        atRestoreInstaceState(savedInstanceState);
        atCreateView(root);

        return root;
    }
}
