package py.com.klezlab.maizdroide.fragment;

import android.content.Context;

public interface ForeverScrollerAdapter {
    void atWaiting();
    Object atFetch(Context context);
    void atDataUpdate(Object obj);

}
