package py.com.klezlab.maizdroide.fragment;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.Hook;

public interface FragmentHook extends Hook {
    @NonNull BaseFragment provideInitialFragment();
    @NonNull ArrayList<BaseFragment> provideFragments();
    int provideFrame();
}
