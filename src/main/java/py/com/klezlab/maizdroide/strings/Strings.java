package py.com.klezlab.maizdroide.strings;

import android.support.annotation.Nullable;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Strings {
    public static final String UTF8 = "UTF-8";

    public static final String base64md5(final String s) throws UnsupportedEncodingException {
        return md5(base64(s));
    }

    public static final String base64(final String s, final String encode) throws UnsupportedEncodingException {
        byte[] data = s.getBytes(encode);
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64.trim();
    }

    public static final String base64(final String s) throws UnsupportedEncodingException {
        return base64(s,UTF8);
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";

        try {
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());

            byte messageDigest[] = digest.digest();
            StringBuilder hexString = new StringBuilder();

            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);

                while (h.length() < 2) {
                    h = "0" + h;
                }

                hexString.append(h);
            }

            return hexString.toString();

        }
        catch (NoSuchAlgorithmException e) {
            Logcat.exception("NoSuchAlgorithmException @ Strings::md5()",e);
        }

        return "";
    }

    @Nullable
    public static String extractor(Object object){
        if(object == null){
            return null;
        }

        String value = null;

        if(object instanceof String){
            value = (String) object;
        }
        else if(object instanceof Double){
            final double d = (double) object;
            value = String.valueOf(d);
        }
        else if(object instanceof Integer){
            final int i = (int) object;
            value = String.valueOf(i);
        }
        else if(object instanceof Boolean){
            final boolean b = (boolean) object;
            value = String.valueOf(b);
        }

        return value;
    }
}