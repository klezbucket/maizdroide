package py.com.klezlab.maizdroide.dispatcher;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import py.com.klezlab.maizdroide.activity.ImageViewActivity;
import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.debug.Logcat;

public class Dispatcher {
    public static DispatcherImpl with(MaizActivity activity,boolean finishMe){
        return new DispatcherImpl(activity,finishMe);
    }

    public static DispatcherImpl with(MaizActivity activity) {
        return new DispatcherImpl(activity,false);
    }

    public static class DispatcherImpl {
        private MaizActivity activity;
        private boolean finishMe;
        private Intent intent;
        private int animIn;
        private int animOut;

        public DispatcherImpl(MaizActivity activity, boolean finishMe) {
            this.activity = activity;
            this.finishMe = finishMe;
        }

        public void image(String path){
            jumpTo(ImageViewActivity.class);
            putExtra(ImageViewActivity.EXTRA_PATH,path);
            redirect();
        }

        public void image(String path,String color){
            jumpTo(ImageViewActivity.class);
            putExtra(ImageViewActivity.EXTRA_PATH,path);
            putExtra(ImageViewActivity.EXTRA_COLOR,color);
            redirect();
        }

        public void redirect(){
            if(intent != null) {
                activity.startActivity(intent);
            }

            if(finishMe){
                activity.finish();
            }

            if(animIn != 0 && animOut != 0){
                activity.overridePendingTransition(animIn,animOut);
            }
        }

        public void redirectFallbackUrl(String fallbackUrl){
            try {
                if (intent != null) {
                    activity.startActivity(intent);
                }
            }
            catch(ActivityNotFoundException e){
                Logcat.exception("ActivityNotFoundException @ Dispatcher::redirectFallbackUrl()",e);

                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(fallbackUrl));
                activity.startActivity(intent);
            }

            if(finishMe){
                activity.finish();
            }

            if(animIn != 0 && animOut != 0){
                activity.overridePendingTransition(animIn,animOut);
            }
        }

        public DispatcherImpl transition(int in,int out){
            animIn = in;
            animOut = out;

            return this;
        }

        public DispatcherImpl putFlag(int flag){
            intent.setFlags(flag);
            return this;
        }

        public DispatcherImpl jumpTo(Class dest){
            intent = new Intent(activity,dest);
            return this;
        }

        public DispatcherImpl putExtra(String key, boolean value){
            intent.putExtra(key, value);
            return this;
        }

        public DispatcherImpl putExtra(String key, int value){
            intent.putExtra(key, value);
            return this;
        }

        public DispatcherImpl putExtra(String key, String value){
            intent.putExtra(key, value);
            return this;
        }

        public DispatcherImpl putExtra(String key, long value){
            intent.putExtra(key, value);
            return this;
        }

        public DispatcherImpl putExtra(String key, double value){
            intent.putExtra(key, value);
            return this;
        }

        public void kill() {
            intent = new Intent(Intent.ACTION_MAIN);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addCategory(Intent.CATEGORY_HOME);

            redirect();
        }

        public DispatcherImpl url(String url) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));

            return this;
        }

        public DispatcherImpl call(String telephone) {
            intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse(resolvTelephone(telephone)));

            return this;
        }

        private static String resolvTelephone(String telephone){
            final StringBuilder sb = new StringBuilder("tel:")
                    .append(telephone.replaceAll("[^\\d\\*]",""));

            return sb.toString();
        }

        public DispatcherImpl twitter(String accountName){
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("twitter://user?screen_name=" + accountName.replace("@","")));
            return this;
        }

        public DispatcherImpl facebook(String pageName){
            intent = new Intent(Intent.ACTION_VIEW);

            final PackageManager pm = activity.getPackageManager();
            final String url = "https://facebook.com/" + pageName;

            try {
                final boolean enabled = pm.getApplicationInfo("com.facebook.katana", 0).enabled;
                final int versionCode = pm.getPackageInfo("com.facebook.katana", 0).versionCode;

                if (enabled) {
                    String schema;

                    if (versionCode >= 3002850) {
                        schema = "fb://facewebmodal/f?href=" + url;
                    }
                    else {
                        schema = "fb://page/" + pageName;
                    }

                    intent.setPackage("com.facebook.katana");
                    intent.setData(Uri.parse(schema));
                }
            }
            catch (PackageManager.NameNotFoundException e) {
                Logcat.exception("NameNotFoundException @ Dispatcher::facebook()",e);
                intent.setData(Uri.parse(pageName));
            }

            return this;
        }

        public DispatcherImpl instagram(String accountName){
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://instagram.com/_u/" + accountName.replace("@","")));
            intent.setPackage("com.instagram.android");

            return this;
        }
    }
}
