package py.com.klezlab.maizdroide.keyboard;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Keyboard {
    public static KeyboardImpl with(EditText editText){
        return new KeyboardImpl(editText);
    }

    public static class KeyboardImpl{
        private final EditText editText;
        private final Context  context;

        public KeyboardImpl(EditText editText) {
            this.editText = editText;
            this.context = editText.getContext();
        }

        public void hide(){
            editText.clearFocus();

            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }

        public void show(){
            editText.requestFocus();
            editText.setSelection(editText.getText().length());

            final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }
    }
}
