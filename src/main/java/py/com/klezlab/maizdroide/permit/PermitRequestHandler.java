package py.com.klezlab.maizdroide.permit;

public interface PermitRequestHandler {
    void atRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
}
