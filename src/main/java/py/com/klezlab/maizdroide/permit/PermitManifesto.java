package py.com.klezlab.maizdroide.permit;

import android.os.Parcel;
import android.os.Parcelable;

import py.com.klezlab.maizdroide.activity.MaizActivity;

abstract public class PermitManifesto {
    private final String permit;
    private final MaizActivity activity;

    public abstract void atDenied();
    public abstract void atGranted();

    public PermitManifesto(MaizActivity activity,String permit) {
        this.permit = permit;
        this.activity = activity;
    }

    public String getPermit() {
        return permit;
    }

    public MaizActivity getActivity() {
        return activity;
    }
}
