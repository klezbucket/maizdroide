package py.com.klezlab.maizdroide.permit;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.MaizActivity;

public abstract class PermitAbstractEngine implements PermitHook, PermitRequestHandler {
    private static final String PERMITS_BUNDLE = "PermitAbstractEngine.PERMITS_BUNDLE";
    private final MaizActivity activity;

    public PermitAbstractEngine(MaizActivity activity){
        this.activity = activity;
    }

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {

    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }

    public void request() {
        final ArrayList<PermitManifesto> permitManifestos = providePermits();

        final boolean grantedAll = hasGrantedAll();
        final String[] permitsNames = new String[permitManifestos.size()];
        int i = 0;

        for(PermitManifesto permitManifesto : permitManifestos){
            final String permitName = permitManifesto.getPermit();
            permitsNames[i] = permitName;
            i++;
        }

        if(grantedAll){
            for(PermitManifesto permitManifesto : permitManifestos){
                permitManifesto.atGranted();
            }
        }
        else {
            ActivityCompat.requestPermissions(activity, permitsNames, provideRequestCode());
        }
    }

    public boolean hasGrantedAll(){
        final ArrayList<PermitManifesto> permitManifestos = providePermits();
        boolean grantedAll = true;

        for(PermitManifesto permitManifesto : permitManifestos){
            grantedAll &= check(permitManifesto.getPermit());
        }

        return grantedAll;
    }


    public boolean check(String permit){
        return ContextCompat.checkSelfPermission(activity,permit) == PackageManager.PERMISSION_GRANTED;
    }

    public void callback(String[] permissions, int[] grantResults) {
        final ArrayList<PermitManifesto> permitManifestos = providePermits();

        for(int i = 0;i < grantResults.length;i++){
            final int result = grantResults[i];
            final String permission = permissions[i];
            final PermitManifesto permitManifesto = permitManifestos.get(i);

            if(permission.equals(permitManifesto.getPermit())) {
                if (result == PackageManager.PERMISSION_DENIED) {
                    permitManifesto.atDenied();
                }
                else{
                    permitManifesto.atGranted();
                }
            }
        }
    }

    @Override
    public void atRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(provideRequestCode() != requestCode){
            return;
        }

        callback(permissions,grantResults);
    }
}
