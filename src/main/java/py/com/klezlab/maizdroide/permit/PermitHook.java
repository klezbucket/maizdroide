package py.com.klezlab.maizdroide.permit;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.Hook;

public interface PermitHook extends Hook {
    @NonNull ArrayList<PermitManifesto> providePermits();
    int provideRequestCode();
}
