package py.com.klezlab.maizdroide.alert;

public abstract class AlertEngine implements AlertManifesto {
    public void show(){
        Alert.with(this).show();
    }
}
