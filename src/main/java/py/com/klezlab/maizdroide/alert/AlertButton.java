package py.com.klezlab.maizdroide.alert;

import android.content.DialogInterface;

public class AlertButton {
    final public DialogInterface.OnClickListener clicker;
    final public String label;

    public AlertButton(DialogInterface.OnClickListener clicker, String label) {
        this.clicker = clicker;
        this.label = label;
    }
}
