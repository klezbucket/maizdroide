package py.com.klezlab.maizdroide.alert;

import android.content.Context;

public interface AlertManifesto {
    boolean provideCancelable();
    String provideMessage();
    String provideTitle();
    AlertButton provideNegativeButton();
    AlertButton providePositiveButton();
    Context provideContext();
}
