package py.com.klezlab.maizdroide.alert;

import android.app.AlertDialog;
import android.content.Context;
import android.view.WindowManager;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Alert {
    public static AlertImpl with(Context context){
        return new AlertImpl(context);
    }

    public static AlertImpl with(AlertManifesto factory){
        return new AlertImpl(factory);
    }

    public static class AlertImpl {
        private final AlertDialog.Builder builder;
        private AlertDialog dialog;

        public AlertImpl(Context context) {
            this.builder = new AlertDialog.Builder(context);
        }

        public AlertImpl(AlertManifesto factory) {
            this(factory.provideContext());

            setTitle(factory.provideTitle());
            setCancelable(factory.provideCancelable());
            setMessage(factory.provideMessage());
            setPositiveButton(factory.providePositiveButton());
            setNegativeButton(factory.provideNegativeButton());
        }

        public AlertImpl setTitle(String str) {
            builder.setTitle(str);
            return this;
        }

        public AlertImpl setMessage(String str) {
            builder.setMessage(str);
            return this;
        }

        public AlertImpl setCancelable(boolean cancelable) {
            builder.setCancelable(cancelable);
            return this;
        }

        public AlertImpl setPositiveButton(AlertButton button) {
            if(button != null) {
                builder.setPositiveButton(button.label, button.clicker);
            }

            return this;
        }

        public AlertImpl setNegativeButton(AlertButton button) {
            if(button != null) {
                builder.setNegativeButton(button.label, button.clicker);
            }

            return this;
        }

        public void dismiss(){
            if(dialog == null){
                return;
            }

            dialog.dismiss();
        }

        public void show(){
            try {
                dialog = builder.show();
            }
            catch (WindowManager.BadTokenException e){
                Logcat.exception("BadTokenException @ Alert::show()",e);
            }
        }
    }
}
