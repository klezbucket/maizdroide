package py.com.klezlab.maizdroide.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import py.com.klezlab.maizdroide.debug.Logcat;

public class InputStreamUtils {
    public static InputStreamUtilsImpl with(InputStream stream){
        return new InputStreamUtilsImpl(stream);
    }

    public static class InputStreamUtilsImpl{

        private final InputStream stream;

        public InputStreamUtilsImpl(InputStream stream) {
            this.stream = stream;
        }

        public String toString(String charset){
            StringBuilder sb = new StringBuilder();
            BufferedReader reader;
            String line;

            try {
                reader = new BufferedReader(new InputStreamReader(stream,charset));

                while((line = reader.readLine()) != null){
                    sb.append(line);
                }
            }
            catch (UnsupportedEncodingException e) {
                Logcat.exception("UnsupportedEncodingException @ InputStreamUtils::toString()",e);
            }
            catch (IOException e) {
                Logcat.exception("IOException @ InputStreamUtils::toString()", e);
            }

            return sb.toString();
        }
    }
}
