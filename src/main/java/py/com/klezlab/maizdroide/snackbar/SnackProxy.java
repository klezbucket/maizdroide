package py.com.klezlab.maizdroide.snackbar;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import py.com.klezlab.maizdroide.persistent.Persistent;

public class SnackProxy {
    private final static String SNACK_FILE = "pending_snack";
    private final static String MESSAGE_FIELD = "message";
    private final static String DURATION_FIELD = "duration";

    public static SnackProxyImpl with(Context context){
        return new SnackProxyImpl(context);
    }

    public static class SnackProxyImpl{
        private final Context context;

        public SnackProxyImpl(Context context) {
            this.context = context;
        }

        public void delegate(String message,int duration){
            Persistent.with(context,SNACK_FILE).write(MESSAGE_FIELD,message);
            Persistent.with(context,SNACK_FILE).write(DURATION_FIELD,duration);
        }

        public void consume(CoordinatorLayout layout){
            final String message = Persistent.with(context,SNACK_FILE).read(MESSAGE_FIELD);
            final int duration = Persistent.with(context,SNACK_FILE).readInt(DURATION_FIELD);

            if(message == null){
                return;
            }

            Persistent.with(context,SNACK_FILE).write(MESSAGE_FIELD,null);
            Persistent.with(context,SNACK_FILE).write(DURATION_FIELD,0);
            Snackbar.make(layout,message,duration).show();
        }
    }
}
