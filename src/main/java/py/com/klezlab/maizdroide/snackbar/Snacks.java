package py.com.klezlab.maizdroide.snackbar;

import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

public class Snacks {
    public static SnacksImpl with(CoordinatorLayout coordinator){
        return new SnacksImpl(coordinator);
    }

    public static class SnacksImpl{
        private final CoordinatorLayout coordinator;
        private Snackbar snackbar;

        public SnacksImpl(CoordinatorLayout coordinator) {
            this.coordinator = coordinator;
        }

        public SnacksImpl forShort(int text){
            dismiss();
            snackbar = Snackbar.make(coordinator, text, Snackbar.LENGTH_SHORT);
            return this;
        }
        public SnacksImpl forLong(int text){
            dismiss();
            snackbar = Snackbar.make(coordinator, text, Snackbar.LENGTH_LONG);
            return this;
        }

        public SnacksImpl indeterminate(int text){
            dismiss();
            snackbar = Snackbar.make(coordinator, text, Snackbar.LENGTH_INDEFINITE);

            return this;
        }

        public void dismiss() {
            if(snackbar == null){
                return;
            }

            snackbar.dismiss();
        }

        public SnacksImpl setAction(int text, final View.OnClickListener clicker){
            if(snackbar != null){
                snackbar.setAction(text,clicker);
            }

            return this;
        }

        public SnacksImpl show(){
            if(snackbar != null) {
                snackbar.show();
                final View layout = snackbar.getView();

                layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        ViewGroup.LayoutParams lp = layout.getLayoutParams();

                        if (lp instanceof CoordinatorLayout.LayoutParams) {
                            ((CoordinatorLayout.LayoutParams) lp).setBehavior(new DisableSwipeBehavior());
                            layout.setLayoutParams(lp);
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        else {
                            layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
                });
            }

            return this;
        }
    }
}
