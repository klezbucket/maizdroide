package py.com.klezlab.maizdroide.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import py.com.klezlab.maizdroide.item.BaseItem;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    final private View itemView;

    public BaseViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;

        atCreate(itemView);
    }

    public abstract void atCreate(View root);
    public abstract void atBind(View root, BaseItem item);

    public View getItemView() {
        return itemView;
    }
}