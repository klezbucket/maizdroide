package py.com.klezlab.maizdroide.exception;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.dispatcher.Dispatcher;

public abstract class UncaughtExceptionAbstractEngine implements UncaughtExceptionHook {
    @Override
    public void atCreate(final MaizActivity activity, Bundle savedInstanceState) {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Logcat.exception("UncaughtException",paramThrowable);

                Dispatcher.with(activity,true)
                    .jumpTo(provideCrashFallbackActivity())
                    .redirect();

                System.exit(0);
            }
        });
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
