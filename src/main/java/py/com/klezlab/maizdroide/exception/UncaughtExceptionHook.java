package py.com.klezlab.maizdroide.exception;

import py.com.klezlab.maizdroide.activity.Hook;

public interface UncaughtExceptionHook extends Hook {
    Class provideCrashFallbackActivity();
}
