package py.com.klezlab.maizdroide.debug;

import android.util.Log;

public class Logcat {
    public static String TAG = "MAIZDROIDE";
    public static boolean ENABLED = true;

    public static void initialize(String tag,boolean enabled){
        TAG = tag;
        ENABLED = enabled;
    }

    public static void debug(String msg){
        if(ENABLED)
            Log.i(TAG,msg);
    }

    public static void forced(String msg){
        Log.i(TAG,msg);
    }


    public static void exception(String where,Exception e){
        if(ENABLED)
            Log.e(TAG,where,e);
    }

    public static void exception(String where,Throwable t){
        if(ENABLED)
            Log.e(TAG,where,t);
    }
}
