package py.com.klezlab.maizdroide.debug;

import py.com.klezlab.maizdroide.activity.Hook;

public interface LogcatHook extends Hook {
    boolean provideLogcatAvailability();
    String provideLogcatTag();
}