package py.com.klezlab.maizdroide.debug;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import py.com.klezlab.maizdroide.activity.MaizActivity;

public abstract class LogcatAbstractEngine implements LogcatHook {
    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        Logcat.initialize(provideLogcatTag(),provideLogcatAvailability());
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}