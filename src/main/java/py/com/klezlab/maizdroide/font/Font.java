package py.com.klezlab.maizdroide.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.widget.TextView;

public class Font {
    public static FontImpl with(@NonNull TextView textView){
        return new FontImpl(textView);
    }

    public static class FontImpl {
        private final TextView textView;

        public FontImpl(TextView textView) {
            this.textView = textView;
        }

        public void set(String font){
            final Context ctx = textView.getContext();
            final Typeface tf = FontCache.get(font,ctx);

            if(tf == null){
                return;
            }

            textView.setTypeface(tf);
        }
    }
}
