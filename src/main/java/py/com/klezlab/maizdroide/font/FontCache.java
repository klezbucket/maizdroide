package py.com.klezlab.maizdroide.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;

import java.util.Hashtable;

public class FontCache {
    private static Hashtable<String, Typeface> CACHE = new Hashtable<String, Typeface>();

    @Nullable
    public static Typeface get(String name, Context context) {
        Typeface tf = CACHE.get(name);

        if(tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
            }
            catch (Exception e) {
                return null;
            }

            CACHE.put(name, tf);
        }

        return tf;
    }
}