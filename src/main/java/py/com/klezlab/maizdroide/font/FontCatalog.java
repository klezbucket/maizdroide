package py.com.klezlab.maizdroide.font;

public class FontCatalog {
    public static final String OPEN_SANS_REGULAR = "fonts/OpenSans-Regular.ttf";
    public static final String OPEN_SANS_BOLD = "fonts/OpenSans-Bold.ttf";
}
