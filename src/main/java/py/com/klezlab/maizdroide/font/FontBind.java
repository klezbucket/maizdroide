package py.com.klezlab.maizdroide.font;

public class FontBind {
    public final String font;
    public final int id;

    public FontBind(String font, int id) {
        this.font = font;
        this.id = id;
    }
}
