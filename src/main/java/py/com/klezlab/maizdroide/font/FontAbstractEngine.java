package py.com.klezlab.maizdroide.font;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.MaizActivity;

public abstract class FontAbstractEngine implements FontHook {
    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {

    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity,Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {
        ArrayList<FontBind> binds = provideFontBinds();

        for(FontBind bind : binds){
            final TextView textView = (TextView) activity.findViewById(bind.id);
            Font.with(textView).set(bind.font);
        }
    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
