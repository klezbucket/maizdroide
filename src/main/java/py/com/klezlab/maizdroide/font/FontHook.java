package py.com.klezlab.maizdroide.font;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.activity.Hook;

public interface FontHook extends Hook {
    @NonNull
    ArrayList<FontBind> provideFontBinds();
}