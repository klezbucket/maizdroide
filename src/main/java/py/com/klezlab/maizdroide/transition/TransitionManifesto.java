package py.com.klezlab.maizdroide.transition;

public interface TransitionManifesto {
    int provideFacade();
    int provideOverview();
    long provideDelay();
}
