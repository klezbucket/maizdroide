package py.com.klezlab.maizdroide.transition;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;

import py.com.klezlab.maizdroide.activity.Hook;
import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.thread.AsyncObserver;
import py.com.klezlab.maizdroide.thread.AsyncThread;

abstract public class ButtonAsyncTransition implements Hook {
    public abstract Button provideButton();
    public abstract AsyncThread provideAsync();
    public abstract TransitionManifesto provideTransitionManifesto();

    @Override
    public void atCreate(final MaizActivity activity, Bundle savedInstanceState) {
        final Button button = provideButton();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicker(activity);
            }
        });
    }

    private void clicker(MaizActivity activity) {
        final TransitionManifesto transitionManifesto = provideTransitionManifesto();
        final View facade = activity.findViewById(transitionManifesto.provideFacade());
        final View overview = activity.findViewById(transitionManifesto.provideOverview());
        transition(activity,facade,overview);
    }

    private void transition(final MaizActivity activity, final View facade, final View overview){
        animate(activity,facade,overview,true);
    }

    private void restore(final MaizActivity activity, final View facade, final View overview){
        animate(activity,facade,overview,false);
    }

    private void animate(final MaizActivity activity, final View facade, final View overview, final boolean trigger){
        final TransitionManifesto transitionManifesto = provideTransitionManifesto();
        final AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeOut.setDuration(transitionManifesto.provideDelay());

        final AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        fadeIn.setDuration(transitionManifesto.provideDelay());

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                facade.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                overview.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(trigger) {
                    asyncTrigger(activity);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        overview.startAnimation(fadeIn);
        facade.startAnimation(fadeOut);
    }

    private void asyncTrigger(final MaizActivity activity) {
        final AsyncThread async = provideAsync();
        final AsyncObserver observer = new AsyncObserver(async) {
            @Override
            public void onFinished(AsyncThread async) {
                final TransitionManifesto transitionManifesto = provideTransitionManifesto();
                final View facade = activity.findViewById(transitionManifesto.provideFacade());
                final View overview = activity.findViewById(transitionManifesto.provideOverview());
                restore(activity,overview,facade);
            }
        };

        observer.run();
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity,Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
