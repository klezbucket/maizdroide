package py.com.klezlab.maizdroide.netimages;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.io.File;

import py.com.klezlab.maizdroide.file.BitmapWriter;
import py.com.klezlab.maizdroide.netimages.GlideFetcher.GlideFetcherImpl;

public abstract class GlideRemote implements GlideCallback {
    private final GlideFetchManifesto manifesto;
    private final ImageView view;
    private GlideFetcherImpl remote;
    private GlideFetcherImpl locale;

    protected GlideRemote(GlideFetchManifesto manifesto, ImageView view) {
        this.manifesto = manifesto;
        this.view = view;
    }

    public void fetch(){
        if(isRemoteNeeded()){
            remote();
        }
        else{
            locale();
        }
    }

    public boolean hasLocalPhoto(){
        final File local = new File(manifesto.sql.getLocal());
        return local.exists();
    }

    public String getLocalPath() {
        return manifesto.sql.getLocal();
    }

    private boolean isRemoteNeeded() {
        final long expiration = manifesto.expiration;
        final long fetched = manifesto.sql.getFetched();
        final long now = System.currentTimeMillis();
        final File local = new File(manifesto.sql.getLocal());

        if(local.exists() == false){
            return true;
        }

        if(now - fetched > expiration){
            return true;
        }

        return false;
    }

    private void remote(){
        if(hasLocalPhoto()){
            localRemote();
        }
        else{
            fullRemote();
        }
    }

    private void fullRemote() {
        if(remote == null || remote.isFinished()) {
            remote = GlideFetcher.remote(manifesto);
            beforeRemoteRender(remote);
            remote.into(view,GlideRemote.this);
        }
    }

    private void localRemote() {
        if(locale == null || locale.isFinished()) {
            locale = GlideFetcher.local(manifesto);
            beforeLocalRender(locale);

            locale.into(view, new GlideCallback() {
                @Override
                public void atBitmapFetch(Bitmap bitmap) {
                    fullRemote();
                }
            });
        }
    }

    private void locale() {
        if(locale == null || locale.isFinished()){
            locale = GlideFetcher.local(manifesto);
            beforeLocalRender(locale);
            locale.into(view);
        }
    }

    public GlideFetchManifesto getManifesto() {
        return manifesto;
    }

    public ImageView getView() {
        return view;
    }

    @Override
    public final void atBitmapFetch(Bitmap bitmap) {
        BitmapWriter.with(manifesto.sql.getLocal()).write(bitmap);

        atFetch(bitmap);
    }

    abstract public void atFetch(Bitmap bitmap);
    abstract public void beforeLocalRender(GlideFetcherImpl locale);
    abstract public void beforeRemoteRender(GlideFetcherImpl remote);
}
