package py.com.klezlab.maizdroide.netimages;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import java.io.File;
import java.lang.ref.WeakReference;

import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.file.BitmapWriter;
import py.com.klezlab.maizdroide.graphic.Graphic;
import py.com.klezlab.maizdroide.http.BaseClient;
import py.com.klezlab.maizdroide.netimages.sql.NetImagesSql;
import py.com.klezlab.maizdroide.sql.VersionHandler;

public class PicassoFetcher {
    public static PicassoFetcherImpl with(PicassoClient client){
        return new PicassoFetcherImpl(client);
    }

    public static class PicassoFetcherImpl{
        private final Context context;
        private String local;
        private NetImagesSql fetcher;
        private ImageView view;
        private FetchTarget target;
        private PicassoClient client;
        private boolean picassoDiskFetch = false;
        private boolean centerCrop = false;
        private boolean rounded = false;

        public PicassoFetcherImpl setPicassoDiskFetch(boolean picassoDiskFetch) {
            this.picassoDiskFetch = picassoDiskFetch;
            return this;
        }

        public boolean isPicassoDiskFetch() {
            return picassoDiskFetch;
        }

        public PicassoFetcherImpl(PicassoClient client) {
            this.client = client;
            this.context = client.getContext();
        }

        public PicassoFetcherImpl persist(VersionHandler version, String local){
            final String url = client.provideUrl();

            if(url == null){
                return this;
            }

            fetcher = new NetImagesSql(version,context);
            fetcher.findById(url);
            client.bindFetcher(fetcher);

            this.local = local;
            return this;
        }

        public PicassoFetcherImpl into(ImageView view){
            this.view = view;
            return this;
        }

        public PicassoFetcherImpl fetch(long expiration){
            if(client == null){
                return this;
            }

            if(fetcher == null) {
                Logcat.debug("Fetcher is null @ PicassoFetcherImp::fetch");
                return this;
            }

            fetcher.setRemote(client.provideUrl());

            if(fetcher.getLocal() == null) {
                fetcher.setLocal(local);
            }

            if(fetcher.hasExpired(expiration)){
                download();
            }
            else if(picassoDiskFetch) {
                PicassoFetcher.picassoDiskRead(view,fetcher.getLocal(),client);
            }
            else{
                PicassoFetcher.diskRead(this);
            }

            return this;
        }

        private void download() {
            Picasso picasso = new Picasso.Builder(context)
                    .loggingEnabled(Logcat.ENABLED)
                    .downloader(client)
                    .build();

            final String url = client.provideUrl();
            final Uri uri = url == null ? null : Uri.parse(url);

            target = new FetchTarget(this);
            target.setPicassoDiskFetch(isPicassoDiskFetch());

            final RequestCreator request = picasso.load(uri);
            setRequestDrawables(request, client);

            request.noFade();
            request.into(target);
        }

        public PicassoFetcherImpl centerCrop(boolean b) {
            return this;
        }

        public PicassoFetcherImpl rounded(boolean b) {
            return this;
        }
    }

    private static void picassoDiskRead(ImageView view, String local, PicassoClient client) {
        if(view == null){
            return;
        }

        if(local == null){
            return;
        }

        final Picasso picasso = new Picasso.Builder(view.getContext())
                .loggingEnabled(Logcat.ENABLED)
                .build();

        final RequestCreator request = picasso.load(new File(local));
        setRequestDrawables(request,client);

        request.fit();
        request.noFade();
        request.into(view);
    }

    private static void diskRead(final PicassoFetcherImpl picassoFetcher) {
        final String local = picassoFetcher.fetcher.getLocal();
        final ImageView view = picassoFetcher.view;
        final PicassoClient client = picassoFetcher.client;

        if(view == null){
            return;
        }

        if(local == null){
            return;
        }

        final Context context = view.getContext();
        final BitmapRequestBuilder<String, Bitmap> glide = Glide.with(context)
                .load(local)
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(false);

        if(picassoFetcher.centerCrop){
            glide.centerCrop();
        }

        setRequestDrawables(glide,client);

        glide.into(new BitmapImageViewTarget(view){
            @Override
            protected void setResource(Bitmap resource) {
                if(picassoFetcher.rounded) {
                    final RoundedBitmapDrawable circle = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circle.setCircular(true);
                    view.setImageDrawable(circle);
                }
                else{
                    view.setImageBitmap(resource);
                }
            }
        });
    }

    private static void setRequestDrawables(BitmapRequestBuilder<String, Bitmap> glide, PicassoClient client){
        final Drawable errorDrawable = client.provideErrorDrawable();

        if(errorDrawable != null){
            glide.error(errorDrawable);
        }

        final Drawable placeholderDrawable = client.providePlaceholderDrawable();

        if(placeholderDrawable != null){
            glide.placeholder(placeholderDrawable);
        }
    }

    private static void setRequestDrawables(RequestCreator request, PicassoClient client){
        final Drawable errorDrawable = client.provideErrorDrawable();

        if(errorDrawable != null){
            request.error(errorDrawable);
        }

        final Drawable placeholderDrawable = client.providePlaceholderDrawable();

        if(placeholderDrawable != null){
            request.placeholder(placeholderDrawable);
        }
    }

    public static class FetchTarget implements Target{
        private final NetImagesSql fetcher;
        private final PicassoClient client;
        private final ImageView view;
        private boolean picassoDiskFetch = false;
        private WeakReference<PicassoFetcherImpl> picassoFetcher;

        public FetchTarget(PicassoFetcherImpl picassoFetcher) {
            view = picassoFetcher.view;
            client = picassoFetcher.client;
            fetcher = picassoFetcher.fetcher;
            this.picassoFetcher = new WeakReference<>(picassoFetcher);
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            final int width = view.getWidth();
            final Bitmap scaled = Graphic.getScaledBitmap(bitmap,width);

            if(fetcher.hasLocal()){
                storeBitmap(scaled);
            }

            view.setImageBitmap(scaled);
            fetcher.save(client);
        }

        private void storeBitmap(Bitmap bitmap) {
            BitmapWriter.with(fetcher.getLocal()).write(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            if(client.getStatusCode() != BaseClient.CACHE_HIT_STATUS){
                view.setImageDrawable(errorDrawable);
            }

            if(picassoDiskFetch){
                PicassoFetcher.picassoDiskRead(view,fetcher.getLocal(), client);
            }
            else{
                PicassoFetcher.diskRead(picassoFetcher.get());
            }

            fetcher.saveFetched();
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            view.setImageDrawable(placeHolderDrawable);
        }

        public void setPicassoDiskFetch(boolean picassoDiskFetch) {
            this.picassoDiskFetch = picassoDiskFetch;
        }
    }
}
