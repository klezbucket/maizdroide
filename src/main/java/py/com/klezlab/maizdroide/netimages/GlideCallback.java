package py.com.klezlab.maizdroide.netimages;

import android.graphics.Bitmap;

public interface GlideCallback {
    void atBitmapFetch(final Bitmap bitmap);
}
