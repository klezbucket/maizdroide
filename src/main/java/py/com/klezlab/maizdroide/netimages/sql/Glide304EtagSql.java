package py.com.klezlab.maizdroide.netimages.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteSchema;
import py.com.klezlab.maizdroide.sql.VersionHandler;
import py.com.klezlab.maizdroide.sql.column.Text;
import py.com.klezlab.maizdroide.sql.primary.TextPrimary;

public class Glide304EtagSql extends Sqlite {
    public Glide304EtagSql(VersionHandler version, Context context) {
        super(version, context, Schema.load(context));
    }

    @Override
    public String provideTableName() {
        return getContext().getString(R.string.sql_glide304_etag);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db, int version) {

    }

    @Override
    protected String providePrimaryField() {
        return getContext().getString(R.string.sql_net_images_remote);
    }

    public void setRemote(String value){
        write(getContext().getString(R.string.sql_net_images_remote),value);
    }

    public void setEtag(String value){
        write(getContext().getString(R.string.sql_net_images_etag),value);
    }

    public String getRemote(){
        return read(getContext().getString(R.string.sql_net_images_remote));
    }

    public String getEtag(){
        return read(getContext().getString(R.string.sql_net_images_etag));
    }

    public boolean loadByRemote(String remote) {
        final String REMOTE = getContext().getString(R.string.sql_net_images_remote);
        final SQLiteDatabase database = getReadableDatabase();
        final StringBuilder sb = new StringBuilder()
                .append(REMOTE)
                .append(" = ?");

        final Query query = new Query();
        query.setLimit(1, 0);
        query.setWhere(sb.toString());
        query.setWhereArgs(new String[]{ remote });

        Leakable leakable;

        if(database != null) {
            leakable = find(database, query);
        }
        else{
            leakable = find(query);
        }

        Cursor cursor = leakable.getCursor();

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            load(cursor);
            closeTheLeakable(database,leakable);

            return true;
        }

        closeTheLeakable(database,leakable);
        return false;
    }

    private static class Schema extends SqliteSchema {
        public static Schema load(Context ctx){
            Schema schema = new Schema();
            schema.add(new TextPrimary(ctx.getString(R.string.sql_net_images_remote)));
            schema.add(new Text(ctx.getString(R.string.sql_net_images_etag)));

            return schema;
        }
    }
}
