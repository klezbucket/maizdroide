package py.com.klezlab.maizdroide.netimages;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.netimages.sql.Glide304EtagSql;
import py.com.klezlab.maizdroide.netimages.sql.Glide304SqliteVersion;

public class Glide304Module extends AbstractGlideModule implements Interceptor{
    private static final String ETAG = "Etag";
    private static final String IF_NONE_MATCH = "If-None-Match";

    public OkHttpClient provideClient() {
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(this)
                .build();

        return client;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        final Context context = getContext();
        final int databaseVersion = context.getResources().getInteger(R.integer.glide304_db_version);
        final String databaseName = context.getResources().getString(R.string.glide304_db_name);
        final Glide304SqliteVersion version = new Glide304SqliteVersion(context,databaseName,databaseVersion);
        final Glide304EtagSql sql = new Glide304EtagSql(version,context);

        final Request request = chain.request();
        final String key = generateEtagKey(request);
        final Request.Builder builder = request.newBuilder();

        if(sql.loadByRemote(key)) {
            final String ifNoneMatch = sql.getEtag();

            if (ifNoneMatch != null) {
                builder.addHeader(IF_NONE_MATCH, ifNoneMatch);
            }
        }

        final Response response = chain.proceed(builder.build());
        final String etag = response.header(ETAG);

        if(etag != null) {
            sql.setRemote(key);
            sql.setEtag(etag);
            sql.upsert();
        }

        return response;
    }

    private String generateEtagKey(Request request) {
        final String url = request.url().toString();
        return url;
    }
}