package py.com.klezlab.maizdroide.netimages.sql;

import android.content.Context;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.VersionHandler;

public class Glide304SqliteVersion extends VersionHandler {
    public Glide304SqliteVersion(Context ctx, String name, int version) {
        super(ctx, name, version);
    }

    @Override
    public ArrayList<Sqlite> provideModels(Context ctx) {
        final ArrayList<Sqlite> models = new ArrayList<>();
        models.add(new Glide304EtagSql(this,ctx));

        return models;
    }
}
