package py.com.klezlab.maizdroide.netimages;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;

import java.io.InputStream;

import okhttp3.OkHttpClient;

abstract public class AbstractGlideModule implements GlideModule {
    private Context context;
    public abstract OkHttpClient provideClient();

    @Override public void applyOptions(Context context, GlideBuilder builder) {

    }

    @Override public void registerComponents(Context context, Glide glide) {
        this.context = context;
        glide.register(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(provideClient()));
    }

    public Context getContext(){
        return context;
    }
}
