package py.com.klezlab.maizdroide.netimages;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.squareup.picasso.Downloader;

import java.io.IOException;

import py.com.klezlab.maizdroide.http.BaseClient;
import py.com.klezlab.maizdroide.http.Streamer;
import py.com.klezlab.maizdroide.netimages.sql.NetImagesSql;

public abstract class PicassoClient extends BaseClient implements Downloader, Streamer {
    private final Context context;
    private final String url;

    public PicassoClient(Context context, String url) {
        this.context = context;
        this.url = url;
    }

    @Override
    protected String provideUrl() {
        return url;
    }

    @Override
    public Response load(Uri uri, int networkPolicy) throws IOException {
        query();

        final Response response = new Response(getRawResponse(),false,getContentLength());
        return response;
    }

    @Override
    public void shutdown() {
        close();
    }

    public Context getContext() {
        return context;
    }

    @Nullable public abstract Drawable provideErrorDrawable();
    @Nullable public abstract Drawable providePlaceholderDrawable();
    public abstract void bindFetcher(NetImagesSql fetcher);
}
