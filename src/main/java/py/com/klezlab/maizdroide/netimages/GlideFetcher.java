package py.com.klezlab.maizdroide.netimages;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.widget.ImageView;

import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.File;

import py.com.klezlab.maizdroide.debug.Logcat;
import py.com.klezlab.maizdroide.netimages.sql.NetImagesSql;

public class GlideFetcher {
    public static GlideFetcherImpl with(Context context){
        return new GlideFetcherImpl(context);
    }

    public static GlideFetcherImpl with(GlideFetchManifesto manifesto,ImageView view){
        final GlideFetcherImpl locale = local(manifesto);
        locale.into(view);

        return remote(manifesto);
    }

    public static GlideFetcherImpl local(GlideFetchManifesto manifesto){
        final GlideFetcherImpl fetcher = new GlideFetcherImpl(manifesto.context);
        initializeLocal(fetcher,manifesto);

        return fetcher;
    }

    public static GlideFetcherImpl remote(GlideFetchManifesto manifesto){
        final GlideFetcherImpl fetcher = new GlideFetcherImpl(manifesto.context);
        initializeRemote(fetcher,manifesto);

        return fetcher;
    }

    private static void initializeLocal(GlideFetcherImpl fetcher, GlideFetchManifesto manifesto) {
        final NetImagesSql sql = manifesto.sql;
        final String local = sql.getLocal();
        fetcher.loadBitmap(local);
        fetcher.cache(DiskCacheStrategy.NONE,true);
    }

    private static void initializeRemote(GlideFetcherImpl fetcher, GlideFetchManifesto manifesto) {
        final NetImagesSql sql = manifesto.sql;
        final String remote = sql.getRemote();
        fetcher.loadBitmap(remote);
        fetcher.cache(DiskCacheStrategy.NONE,true);
    }

    public static class GlideFetcherImpl {
        private final Context context;
        private final RequestManager glide;
        private BitmapTypeRequest<?> bitmapRequest;
        private boolean roundBitmap;
        private GlideCallback callback;
        private GlideErrorCallback errorCallback;
        public boolean finished;
        private SimpleTarget<Bitmap> target;

        public boolean isFinished() {
            return finished;
        }

        public GlideFetcherImpl(Context context) {
            this.context = context;
            this.glide = Glide.with(context);
        }

        public GlideFetcherImpl errorCallback(GlideErrorCallback callback) {
            this.errorCallback = callback;
            return this;
        }

        public GlideFetcherImpl callback(GlideCallback callback) {
            this.callback = callback;
            return this;
        }

        public GlideFetcherImpl loadBitmap(GlideUrl glideUrl) {
            bitmapRequest = glide.load(glideUrl).asBitmap();
            return this;
        }

        public GlideFetcherImpl loadBitmap(File file) {
            bitmapRequest = glide.load(file).asBitmap();
            return this;
        }

        public GlideFetcherImpl loadBitmap(String path) {
            bitmapRequest = glide.load(path).asBitmap();
            return this;
        }

        public GlideFetcherImpl loadBitmap(int resId) {
            bitmapRequest = glide.load(resId).asBitmap();
            return this;
        }

        public GlideFetcherImpl loadBitmap(Uri uri) {
            bitmapRequest = glide.load(uri).asBitmap();
            return this;
        }

        public GlideFetcherImpl cache(DiskCacheStrategy strategy, boolean skipMemoryCache) {
            bitmapRequest.diskCacheStrategy(strategy)
                    .skipMemoryCache(skipMemoryCache);

            return this;
        }

        public GlideFetcherImpl fitCenter() {
            bitmapRequest.fitCenter();
            return this;
        }

        public GlideFetcherImpl centerCrop() {
            bitmapRequest.centerCrop();
            return this;
        }

        public GlideFetcherImpl override(int w,int h) {
            bitmapRequest.override(w,h);
            return this;
        }

        public GlideFetcherImpl placeholder(int res) {
            bitmapRequest.placeholder(res);
            return this;
        }

        public GlideFetcherImpl error(int res) {
            bitmapRequest.error(res);
            return this;
        }

        public GlideFetcherImpl placeholder(@Nullable Drawable drawable) {
            if (drawable != null) {
                bitmapRequest.placeholder(drawable);
            }

            return this;
        }

        public GlideFetcherImpl error(@Nullable Drawable drawable) {
            if (drawable != null) {
                bitmapRequest.error(drawable);
            }

            return this;
        }

        public GlideFetcherImpl rounded() {
            this.roundBitmap = true;
            return this;
        }

        public void into(GlideCallback callback) {
            callback(callback);

            final ImageView view = null;
            into(view);
        }

        public void into(@Nullable final ImageView view,GlideCallback callback) {
            callback(callback);
            into(view);
        }

        public void into(@Nullable final ImageView view) {
            finished = false;

            target = new SimpleTarget<Bitmap>() {
                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    Logcat.exception("Exception @ GlideFetcher::into()", e);

                    if (errorCallback != null) {
                        errorCallback.atLoadError(errorDrawable);
                    } else if (errorDrawable != null) {
                        super.onLoadFailed(e, errorDrawable);
                    }

                    if(target != null) {
                        Glide.clear(target);
                    }

                    finishing();
                }

                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    if (callback != null) {
                        callback.atBitmapFetch(resource);
                    }

                    if (roundBitmap) {
                        renderRound(resource, view);
                    } else {
                        renderNormal(resource, view);
                    }

                    finishing();
                }
            };

            bitmapRequest.into(target);
        }

        private void finishing() {
            finished = true;
        }

        private void renderNormal(Bitmap resource, @Nullable ImageView view) {
            if(view == null){
                return;
            }

            view.setImageBitmap(resource);
        }

        private void renderRound(Bitmap resource, @Nullable ImageView view) {
            if(view == null){
                return;
            }

            final RoundedBitmapDrawable circle = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
            circle.setCircular(true);
            view.setImageDrawable(circle);
        }
    }
}
