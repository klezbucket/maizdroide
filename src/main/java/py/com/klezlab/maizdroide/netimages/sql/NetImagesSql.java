package py.com.klezlab.maizdroide.netimages.sql;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.netimages.PicassoClient;
import py.com.klezlab.maizdroide.sql.Sqlite;
import py.com.klezlab.maizdroide.sql.SqliteSchema;
import py.com.klezlab.maizdroide.sql.VersionHandler;
import py.com.klezlab.maizdroide.sql.column.BigInteger;
import py.com.klezlab.maizdroide.sql.column.Text;
import py.com.klezlab.maizdroide.sql.primary.TextPrimary;

public class NetImagesSql extends Sqlite {
    public NetImagesSql(VersionHandler version, Context context) {
        super(version, context, Schema.load(context));
    }

    @Override
    public String provideTableName() {
        return getContext().getString(R.string.sql_net_images_table);
    }

    @Override
    public void upgradeTable(SQLiteDatabase db, int version) {

    }

    @Override
    protected String providePrimaryField() {
        return getContext().getString(R.string.sql_net_images_remote);
    }

    public void setRemote(String value){
        write(getContext().getString(R.string.sql_net_images_remote),value);
    }

    public void setLocal(String value){
        write(getContext().getString(R.string.sql_net_images_local),value);
    }

    public void setFetched(long value){
        write(getContext().getString(R.string.sql_net_images_fetched), String.valueOf(value));
    }

    public void setEtag(String value){
        write(getContext().getString(R.string.sql_net_images_etag),value);
    }

    public void setLastModified(String value){
        write(getContext().getString(R.string.sql_net_images_last_modified),value);
    }

    public String getRemote(){
        return read(getContext().getString(R.string.sql_net_images_remote));
    }

    public String getLocal(){
        return read(getContext().getString(R.string.sql_net_images_local));
    }

    public long getFetched(){
        final String fetched = read(getContext().getString(R.string.sql_net_images_fetched));

        if(fetched == null){
            return 0;
        }

        return Long.parseLong(fetched);
    }

    public String getEtag(){
        return read(getContext().getString(R.string.sql_net_images_etag));
    }

    public String getLastModified(){
        return read(getContext().getString(R.string.sql_net_images_last_modified));
    }

    public boolean hasExpired(long expiration) {
        final long current = System.currentTimeMillis();
        final long fetched = getFetched();
        final long diff = current - fetched;

        return diff > expiration;
    }

    public boolean hasLocal() {
        return getLocal() != null;
    }

    public void save(PicassoClient client) {
        setFetched(System.currentTimeMillis());
        setEtag(client.getEtag());
        setLastModified(client.getLastModified());
        upsert();
    }

    public void saveFetched() {
        setFetched(System.currentTimeMillis());
        upsert();
    }

    public boolean loadByRemote(String remote) {
        final String REMOTE = getContext().getString(R.string.sql_net_images_remote);
        final SQLiteDatabase database = getReadableDatabase();
        final StringBuilder sb = new StringBuilder()
                .append(REMOTE)
                .append(" = ?");

        final Query query = new Query();
        query.setLimit(1, 0);
        query.setWhere(sb.toString());
        query.setWhereArgs(new String[]{ remote });

        Leakable leakable;

        if(database != null) {
            leakable = find(database, query);
        }
        else{
            leakable = find(query);
        }

        Cursor cursor = leakable.getCursor();

        if(cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            load(cursor);
            closeTheLeakable(database,leakable);

            return true;
        }

        closeTheLeakable(database,leakable);
        return false;
    }

    private static class Schema extends SqliteSchema {
        public static Schema load(Context ctx){
            Schema schema = new Schema();
            schema.add(new TextPrimary(ctx.getString(R.string.sql_net_images_remote)));
            schema.add(new Text(ctx.getString(R.string.sql_net_images_local)));
            schema.add(new BigInteger(ctx.getString(R.string.sql_net_images_fetched)));
            schema.add(new Text(ctx.getString(R.string.sql_net_images_etag)));
            schema.add(new Text(ctx.getString(R.string.sql_net_images_last_modified)));

            return schema;
        }
    }
}
