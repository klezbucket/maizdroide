package py.com.klezlab.maizdroide.netimages;

import android.graphics.drawable.Drawable;

public interface GlideErrorCallback {
    void atLoadError(final Drawable errorDrawable);
}
