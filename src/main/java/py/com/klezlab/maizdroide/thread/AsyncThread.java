package py.com.klezlab.maizdroide.thread;

import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.Nullable;

import java.util.concurrent.RejectedExecutionException;

import py.com.klezlab.maizdroide.debug.Logcat;

public abstract class AsyncThread extends AsyncTask<Object, Object, Object> {
    private Status status;
    private long delay;
    private AsyncTask<Object, Object, Object> task;

    public enum Status {
        PENDING, RUNNING, FINISHED;
    };

    public AsyncThread() {
        status = Status.PENDING;
    }

    public AsyncThread(long delay) {
        this();
        this.delay = delay;
    }

    protected void setDelay(long delay){
        this.delay = delay;
    }

    protected abstract void afterTask(@Nullable Object obj);
    @Nullable protected abstract Object task(@Nullable Object[] params);
    @Nullable protected abstract Object beforeTask();

    public void run(){
        status = Status.RUNNING;

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                task = executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, beforeTask());
            }
            else {
                task = execute(beforeTask());
            }
        }
        catch(RejectedExecutionException e){
            Logcat.exception("RejectedExecutionException @ AsyncThread::run()",e);
        }
    }

    public void stop(){
        task.cancel(true);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();

        status = Status.FINISHED;
    }

    @Override
    protected void onPostExecute(Object obj) {
        super.onPostExecute(obj);

        afterTask(obj);
        status = Status.FINISHED;
    }


    @Override
    protected Object doInBackground(Object... params) {
        final long b1 = System.currentTimeMillis();
        final Object result = this.task(params);
        final long b2 = System.currentTimeMillis();
        final long resto = b2 - b1;

        if (resto < delay) {
            try {
                Thread.sleep(delay - resto);
            }
            catch (Exception e) {
                Thread.currentThread().interrupt();
                Logcat.exception("InterruptedException @ AsyncThread::doInBackground()", e);
                status = Status.FINISHED;

                return null;
            }
        }

        return result;
    }

    public boolean isPending(){
        return Status.PENDING.equals(status);
    }

    public boolean isRunning(){
        return Status.RUNNING.equals(status);
    }

    public boolean isFinished(){
        return Status.FINISHED.equals(status);
    }
}
