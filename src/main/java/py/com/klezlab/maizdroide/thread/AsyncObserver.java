package py.com.klezlab.maizdroide.thread;

import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.debug.Logcat;

abstract public class AsyncObserver extends AsyncThread {
    private final AsyncThread async;

    public AsyncObserver(AsyncThread async) {
        this.async = async;
    }

    public abstract void onFinished(AsyncThread async);

    @Nullable
    @Override
    protected Object beforeTask() {
        async.run();
        return null;
    }

    @Override
    protected void afterTask(@Nullable Object obj) {
        onFinished(async);
    }

    @Override
    public Object task(Object... params) {
        while(async.isFinished() == false){
            try {
                Thread.sleep(100);
            }
            catch (Exception e) {
                Thread.currentThread().interrupt();
                Logcat.exception("InterruptedException @ AsyncObserver::doInBackground()", e);
                return null;
            }
        }

        return null;
    }
}
