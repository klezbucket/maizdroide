package py.com.klezlab.maizdroide.thread;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.network.Network;

abstract public class NetworkThread extends AsyncThread {
    final MaizActivity activity;

    public NetworkThread(MaizActivity activity, long delay) {
        super(delay);

        this.activity = activity;
    }

    public enum NetworkStatus {
        PENDING, DISCONNECTED, FORBIDDEN, SERVER_ERROR, REQUEST_ERROR, TIMEOUT, NOT_MODIFIED, SUCCESS, INTERRUPTED;
    }

    private NetworkStatus networkStatus;

    public NetworkStatus getNetworkStatus() {
        return networkStatus;
    }

    @Nullable
    @Override
    protected Object beforeTask() {
        if(Network.with(activity).isConnected() == false){
            return NetworkStatus.DISCONNECTED;
        }

        return NetworkStatus.PENDING;
    }

    @Nullable
    @Override
    protected Object task(@Nullable Object[] params) {
        final NetworkStatus status = (NetworkStatus) params[0];

        switch (status) {
            case PENDING:
                return networkTask();
        }

        return status;
    }

    @Override
    protected void afterTask(@Nullable Object obj) {
        networkStatus = (NetworkStatus) obj;

        if(networkStatus == null){
            throw new IllegalStateException("NetworkThread::status == NULL");
        }

        switch (networkStatus){
            case PENDING:
                throw new IllegalStateException("NetworkThread::status == PENDING");
            case DISCONNECTED:
                atDisconnected();
                break;
            case FORBIDDEN:
                atForbidden();
                break;
            case SERVER_ERROR:
                atServerError();
                break;
            case NOT_MODIFIED:
                atNotModified();
                break;
            case REQUEST_ERROR:
                atRequestError();
                break;
            case TIMEOUT:
                atTimeout();
                break;
            case INTERRUPTED:
                atInterrupted();
                break;
            case SUCCESS:
                atSuccess();
                break;
        }
    }

    @NonNull
    protected abstract NetworkStatus networkTask();
    protected abstract void atSuccess();
    protected abstract void atTimeout();
    protected abstract void atServerError();
    protected abstract void atForbidden();
    protected abstract void atRequestError();
    protected abstract void atDisconnected();
    protected abstract void atNotModified();
    protected abstract void atInterrupted();
}
