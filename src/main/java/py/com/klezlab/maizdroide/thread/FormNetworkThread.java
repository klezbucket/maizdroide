package py.com.klezlab.maizdroide.thread;

import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.form2.FormAbstractEngine;

abstract public class FormNetworkThread extends NetworkThread {
    public FormNetworkThread(MaizActivity activity, long delay) {
        super(activity,delay);
    }

    @Nullable
    @Override
    protected Object beforeTask() {
        final FormAbstractEngine formEngine = provideFormEngine();

        if(formEngine.isValid(false)){
            return super.beforeTask();
        }

        setDelay(activity.getResources().getInteger(R.integer.form_error_delay));
        return NetworkStatus.REQUEST_ERROR;
    }

    @Override
    protected void afterTask(@Nullable Object obj) {
        final FormAbstractEngine formEngine = provideFormEngine();
        formEngine.toggleValidations();

        super.afterTask(obj);
    }

    protected abstract FormAbstractEngine provideFormEngine();
}
