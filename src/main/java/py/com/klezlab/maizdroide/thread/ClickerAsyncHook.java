package py.com.klezlab.maizdroide.thread;

import android.view.View;

public interface ClickerAsyncHook extends SingleAsyncHook {
    View provideClickerView();
}
