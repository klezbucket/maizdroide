package py.com.klezlab.maizdroide.thread;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import py.com.klezlab.maizdroide.activity.MaizActivity;

abstract public class ClickerAsyncAbstractEngine implements ClickerAsyncHook {
    private AsyncThread thread;

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        final View view = provideClickerView();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thread = provideAsyncThread();
                thread.run();
            }
        });
    }

    @Override
    public void atStop(MaizActivity activity) {
        if(thread == null){
            return;
        }

        thread.stop();
        thread = null;
    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
