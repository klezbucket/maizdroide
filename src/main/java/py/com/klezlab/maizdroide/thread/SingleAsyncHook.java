package py.com.klezlab.maizdroide.thread;

import py.com.klezlab.maizdroide.activity.Hook;

public interface SingleAsyncHook extends Hook {
    AsyncThread provideAsyncThread();
}
