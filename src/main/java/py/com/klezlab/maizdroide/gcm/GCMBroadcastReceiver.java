package py.com.klezlab.maizdroide.gcm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import py.com.klezlab.maizdroide.debug.Logcat;

public class GCMBroadcastReceiver extends BroadcastReceiver{

    public GCMBroadcastReceiver(){
        super();
        engine = null;
    }

    public static final String TOKEN_RECEIVER = "GCM.Token.Receiver";
    public static final String TOKEN_BROADCAST_ACTION = "GCM.Token.Action";
    private final GCMAbstractEngine engine;

    public GCMBroadcastReceiver(GCMAbstractEngine engine){
        super();
        this.engine = engine;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        Logcat.debug("GCM | Broadcast Received Bundle: " + extras.toString());

        final String token = extras.getString(TOKEN_RECEIVER);

        if(token == null){
            return;
        }

        Logcat.debug("GCM | Broadcast Received Token: " + token);

        if(engine == null){
            return;
        }

        engine.atTokenReceive(token);
    }
}
