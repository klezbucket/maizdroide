package py.com.klezlab.maizdroide.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

abstract public class IIDListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, provideRegistrationServiceClass());
        intent.putExtra(GCMBroadcastReceiver.TOKEN_BROADCAST_ACTION, provideGCMBroadcastAction());
        startService(intent);
    }

    protected abstract String provideGCMBroadcastAction();

    protected abstract Class<?> provideRegistrationServiceClass();
}
