package py.com.klezlab.maizdroide.gcm;

import android.content.BroadcastReceiver;

import py.com.klezlab.maizdroide.activity.Hook;

public interface GCMHook extends Hook {
    Class<?> provideRegistrator();
    void atUnsupportedGooglePlay();
    boolean showUnsupportedGooglePlayDialog();
    boolean doRegistration();
    BroadcastReceiver provideBroadcaster();
    String provideAction();
    void atTokenReceive(String token);
}
