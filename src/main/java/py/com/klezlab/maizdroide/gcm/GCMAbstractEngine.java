package py.com.klezlab.maizdroide.gcm;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.debug.Logcat;

public abstract class GCMAbstractEngine implements GCMHook {
    private final static String REGISTERED_BUNDLE = "GCMAbstractEngine.REGISTERED_BUNDLE";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9001;
    private boolean registered;

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        resolvRegistered(savedInstanceState);
    }

    private void resolvRegistered(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return;
        }

        registered = savedInstanceState.getBoolean(REGISTERED_BUNDLE,false);
    }

    @Override
    public void atStop(MaizActivity activity) {
        try {
            activity.unregisterReceiver(provideBroadcaster());
        }
        catch(Exception e){
            Logcat.exception("Exception @ GCMAbstractEngine::atStop()",e);
        }
    }

    @Override
    public void atResume(MaizActivity activity) {
        broadcastResume(activity);
        setup(activity);
    }

    private void setup(MaizActivity activity) {
        if(registered == false) {
            if (checkPlayServices(activity)) {
                register(activity);
            }
        }
    }

    private boolean checkPlayServices(MaizActivity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                if(showUnsupportedGooglePlayDialog()) {
                    apiAvailability.getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
                }
            }
            else {
                atUnsupportedGooglePlay();
            }

            return false;
        }

        return true;
    }

    private void register(MaizActivity activity) {
        if(doRegistration()) {
            final Intent intent = new Intent(activity, provideRegistrator());
            intent.putExtra(GCMBroadcastReceiver.TOKEN_BROADCAST_ACTION, provideAction());
            activity.startService(intent);
            Logcat.debug("GCM CALL");
        }
    }

    private void broadcastResume(MaizActivity activity) {
        final IntentFilter filter = new IntentFilter(provideAction());
        activity.registerReceiver(provideBroadcaster(), filter);
    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {
        outstate.putBoolean(REGISTERED_BUNDLE,registered);
    }

    @Override
    public void atStart(MaizActivity activity) {

    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}
