package py.com.klezlab.maizdroide.gcm;

import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

abstract public class GCMListenerService extends GcmListenerService {
    @Override
    public void onMessageReceived(String from, Bundle data) {
        messageCallback(from,data);
    }

    protected abstract void messageCallback(String from, Bundle data);
}
