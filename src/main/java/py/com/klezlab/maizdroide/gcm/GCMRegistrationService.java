package py.com.klezlab.maizdroide.gcm;

import android.app.IntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import py.com.klezlab.maizdroide.debug.Logcat;

abstract public class GCMRegistrationService extends IntentService {
    public GCMRegistrationService(String name) {
        super(name);
    }

    public GCMRegistrationService() {
        super(null);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);

        try {
            String action = intent.getStringExtra(GCMBroadcastReceiver.TOKEN_BROADCAST_ACTION);
            String token = instanceID.getToken(provideSenderId(), GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            sendBroadcastToken(token,action);
        }
        catch (IOException e) {
            Logcat.exception("Exception @ GCMRegistrationService",e);
            failureCallback();
        }
    }

    public void sendBroadcastToken(String token,String action){
        Logcat.debug("GCM | Broadcast Token: " + token);

        Intent intent = new Intent();
        intent.putExtra(GCMBroadcastReceiver.TOKEN_RECEIVER, token);
        intent.setAction(action);
        sendBroadcast(intent);
    }

    protected abstract void failureCallback();
    protected abstract String provideSenderId();
}
