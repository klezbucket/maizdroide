package py.com.klezlab.maizdroide.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.permit.PermitRequestHandler;

public abstract class MaizActivity  extends AppCompatActivity{
    private Bundle savedInstanceState;

    @NonNull protected abstract ArrayList<Hook> provideHooks();
    @NonNull protected abstract int provideLayout();
    protected abstract BackPressedHandler provideBackPressedHandler();
    protected abstract PermitRequestHandler providePermitRequestHandler();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for(Hook hook : provideHooks()){
            hook.atActivityResult(this,requestCode,resultCode,data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        final PermitRequestHandler requestHandler = providePermitRequestHandler();

        if(requestHandler == null){
            return;
        }

        requestHandler.atRequestPermissionsResult(requestCode,permissions,grantResults);
    }


    @Override
    public void onBackPressed() {
        final BackPressedHandler handler = provideBackPressedHandler();

        if((handler == null) || (handler.atBackPressed(this) == false)) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(provideLayout());

        for(Hook hook : provideHooks()){
            hook.atCreate(this,savedInstanceState);
        }

        this.savedInstanceState = savedInstanceState;
    }

    @Nullable
    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    @Override
    protected void onResume() {
        super.onResume();

        for(Hook hook : provideHooks()){
            hook.atResume(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        for(Hook hook : provideHooks()){
            hook.atStop(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        for(Hook hook : provideHooks()){
            hook.atStart(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        for(Hook hook : provideHooks()){
            hook.atCreateOptionsMenu(this,menu);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        for(Hook hook : provideHooks()){
            if(hook.atOptionsItemSelected(this,item)){
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        for(Hook hook : provideHooks()){
            hook.atSaveInstanceState(this,outState);
        }
    }
}
