package py.com.klezlab.maizdroide.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public interface Hook {
    void atCreate(MaizActivity activity, Bundle savedInstanceState);

    void atStop(MaizActivity activity);

    void atResume(MaizActivity activity);

    void atSaveInstanceState(MaizActivity activity, Bundle outstate);

    void atStart(MaizActivity activity);

    void atCreateOptionsMenu(MaizActivity activity, Menu menu);

    boolean atOptionsItemSelected(MaizActivity activity, MenuItem item);

    void atActivityResult(MaizActivity activity,int requestCode, int resultCode, Intent data);
}
