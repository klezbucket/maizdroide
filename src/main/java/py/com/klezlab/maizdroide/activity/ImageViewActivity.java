package py.com.klezlab.maizdroide.activity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;

import py.com.klezlab.maizdroide.R;
import py.com.klezlab.maizdroide.netimages.GlideCallback;
import py.com.klezlab.maizdroide.netimages.GlideErrorCallback;
import py.com.klezlab.maizdroide.netimages.GlideFetcher;
import uk.co.senab.photoview.PhotoViewAttacher;


public class ImageViewActivity extends AppCompatActivity {
    public static final String EXTRA_PATH = "Image.EXTRA_PATH";
    public static final String BUNDLE_PATH = "Image.BUNDLE_PATH";
    public static final String EXTRA_COLOR = "Image.EXTRA_COLOR";
    public static final String BUNDLE_COLOR = "Image.BUNDLE_COLOR";
    private static final String DEFAULT_COLOR = "#000000";
    private ImageView image;
    private File file;
    private PhotoViewAttacher attacher;
    private String color;
    private View canvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_image_view);
        paintColor(savedInstanceState);

        if(resolvFile(savedInstanceState)) {
            image = (ImageView) findViewById(R.id.image);
            attacher = new PhotoViewAttacher(image);

            GlideFetcher.with(this)
                    .loadBitmap(file)
                    .cache(DiskCacheStrategy.NONE,true)
                    .errorCallback(new GlideErrorCallback() {
                        @Override
                        public void atLoadError(Drawable errorDrawable) {
                            failure();
                        }
                    })
                    .callback(new GlideCallback() {
                        @Override
                        public void atBitmapFetch(Bitmap bitmap) {
                            image.setImageBitmap(bitmap);
                            attacher.update();
                        }
                    })
                    .into(image);
        }
        else{
            failure();
        }
    }

    private void paintColor(Bundle savedInstanceState) {
        canvas = findViewById(R.id.canvas);
        color = resolvColor(savedInstanceState);
        canvas.setBackgroundColor(Color.parseColor(color));
    }

    private void failure() {
        finish();
    }

    private boolean resolvFile(Bundle savedInstanceState) {
        final String path = resolvPath(savedInstanceState);

        if(path != null){
            file = new File(path);

            if(file.exists()){
                if(file.canRead()){
                    if(file.length() > 0){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private String resolvPath(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return getIntent().getStringExtra(EXTRA_PATH);
        }
        else{
            return savedInstanceState.getString(BUNDLE_PATH);
        }
    }

    private String resolvColor(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            final String color = getIntent().getStringExtra(EXTRA_COLOR);

            if(color == null){
                return DEFAULT_COLOR;
            }

            return color;
        }
        else{
            return savedInstanceState.getString(BUNDLE_PATH);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(BUNDLE_PATH,file.getAbsolutePath());
        outState.putString(BUNDLE_COLOR,color);
        super.onSaveInstanceState(outState);
    }
}
