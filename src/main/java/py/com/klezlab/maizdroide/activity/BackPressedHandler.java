package py.com.klezlab.maizdroide.activity;

public interface BackPressedHandler {
    boolean atBackPressed(MaizActivity activity);
}
