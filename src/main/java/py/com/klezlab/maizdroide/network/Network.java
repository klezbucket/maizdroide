package py.com.klezlab.maizdroide.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Network {
    public static NetworkImpl with(Context ctx){
        return new NetworkImpl(ctx);
    }

    public static class NetworkImpl{
        private final Context context;

        public NetworkImpl(Context context) {
            this.context = context;
        }

        public boolean isConnected(){
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }
    }
}
