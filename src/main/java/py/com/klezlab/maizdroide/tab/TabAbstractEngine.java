package py.com.klezlab.maizdroide.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import py.com.klezlab.maizdroide.activity.Hook;
import py.com.klezlab.maizdroide.activity.MaizActivity;
import py.com.klezlab.maizdroide.adapter.BaseTabFragmentAdapter;
import py.com.klezlab.maizdroide.fragment.BaseFragment;

public abstract class TabAbstractEngine implements Hook {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private int currentPage = -1;

    private ViewPager.OnPageChangeListener pageListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            onHide();
            onShow(position);
        }

        private void onHide() {
            final BaseTabFragmentAdapter adapter = provideTabAdapter();

            if(currentPage < 0 || currentPage > adapter.getCount() - 1){
                return;
            }

            final BaseFragment fragment = (BaseFragment) adapter.getItem(currentPage);

            if(fragment == null){
                return;
            }

            fragment.atHide();
        }

        private void onShow(int position) {
            final BaseTabFragmentAdapter adapter = provideTabAdapter();
            final BaseFragment fragment = (BaseFragment) adapter.getItem(position);
            currentPage = position;

            if(fragment == null){
                return;
            }

            fragment.atShow();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public int getCurrentPage(){
        return currentPage;
    }

    protected abstract BaseTabFragmentAdapter provideTabAdapter();
    protected abstract int provideTabLayout();
    protected abstract int providePageLimit();
    protected abstract int provideViewPager();
    protected abstract int provideInitialIndex();

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        viewPager = (ViewPager) activity.findViewById(provideViewPager());
        viewPager.setOffscreenPageLimit(providePageLimit());
        viewPager.setAdapter(provideTabAdapter());
        viewPager.clearOnPageChangeListeners();
        viewPager.addOnPageChangeListener(pageListener);

        tabLayout = (TabLayout) activity.findViewById(provideTabLayout());
        tabLayout.setupWithViewPager(viewPager);

        for(int i = 0;i < tabLayout.getTabCount();i++){
            final int icon = provideTabAdapter().provideTabIcon(i);

            if(icon > 0) {
                tabLayout.getTabAt(i).setIcon(icon);
            }
        }

        viewPager.setCurrentItem(provideInitialIndex());
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {
    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {

    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        return false;
    }


    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }
}
