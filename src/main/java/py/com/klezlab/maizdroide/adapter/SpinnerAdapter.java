package py.com.klezlab.maizdroide.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import py.com.klezlab.maizdroide.item.SpinnerItem;

abstract public class SpinnerAdapter extends ArrayAdapter<String> {
    private final Context context;
    private ArrayList<SpinnerItem> items;
    public ArrayList<SpinnerItem> getItems() {
        return items;
    }

    protected final void updateList(ArrayList<SpinnerItem> replacement){
        clear();

        for(SpinnerItem item : replacement){
            add(item.getLabel());
        }

        items = replacement;

        notifyDataSetInvalidated();
        notifyDataSetChanged();
    }

    public SpinnerAdapter(Context ctx, int layout, ArrayList<SpinnerItem> items) {
        super(ctx,layout,SpinnerAdapter.toStringArray(items));
        this.context = ctx;
        this.items = items;
    }

    private static ArrayList<String> toStringArray(ArrayList<SpinnerItem> items) {
        ArrayList<String> list = new ArrayList<>();

        for (SpinnerItem item : items) {
            list.add(item.getLabel());
        }

        return list;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        final TextView textView = (TextView) super.getDropDownView(position,convertView,parent);
        customizeDropDownView(textView,position);
        return textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SpinnerItem item = items.get(position);
        return getView(item, parent);
    }

    public SpinnerItem getSpinnerItem(int position) {
        return items.get(position);
    }

    public int getPosition(String value) {
        int i = 0;

        for (SpinnerItem item : items) {
            if (item.getValue().equals(value)) {
                return i;
            }

            i++;
        }

        return 0;
    }

    private View getView(SpinnerItem item, ViewGroup parent) {
        View view = item.getView();

        if (view == null) {
            view = provideView(item,parent);
            item.setView(view);
        }

        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public Context getContext() {
        return context;
    }

    protected abstract void customizeDropDownView(TextView view, int position);
    protected abstract View provideView(SpinnerItem item, ViewGroup parent);

    public boolean exists(@Nullable String value) {
        if(value == null){
            return false;
        }

        final ArrayList<SpinnerItem> items = getItems();

        if(items == null){
            return false;
        }

        for(SpinnerItem item : items){
            String itemValue = item.getValue();

            if(value.equals(itemValue)){
                return true;
            }
        }

        return false;
    }
}