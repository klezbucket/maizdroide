package py.com.klezlab.maizdroide.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import py.com.klezlab.maizdroide.item.BaseItem;
import py.com.klezlab.maizdroide.view.BaseViewHolder;

abstract public class BaseAdapter extends RecyclerView.Adapter {
    private BaseViewHolder viewHolder;

    @Override
    public int getItemViewType(int position) {
        return provideItemType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(provideLayout(viewType), parent, false);
        viewHolder = provideViewHolder(view,viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BaseViewHolder viewHolder = (BaseViewHolder) holder;
        final BaseItem item = provideItem(position);
        viewHolder.atBind(viewHolder.getItemView(),item);
    }

    @Override
    public int getItemCount() {
        return provideItemCount();
    }

    public BaseViewHolder getViewHolder() {
        return viewHolder;
    }

    protected abstract BaseViewHolder provideViewHolder(View view, int viewType);
    protected abstract int provideLayout(int viewType);
    protected abstract int provideItemType(int position);
    protected abstract int provideItemCount();
    protected abstract BaseItem provideItem(int position);
    public abstract boolean isForeverScrolled();
}
