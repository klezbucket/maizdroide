package py.com.klezlab.maizdroide.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import py.com.klezlab.maizdroide.item.BaseItem;
import py.com.klezlab.maizdroide.view.BaseViewHolder;

abstract public class SingleAdapter extends BaseAdapter {
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(provideLayout(viewType), parent, false);
        RecyclerView.ViewHolder holder = new ScrollHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ScrollHolder scrollHolder = (ScrollHolder) holder;
        atBind(scrollHolder.itemView);
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    public class ScrollHolder extends RecyclerView.ViewHolder {
        View itemView;

        ScrollHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
        }
    }

    public abstract void atBind(View view);

    @Override
    protected int provideItemType(int position) {
        return 0;
    }

    @Override
    protected int provideItemCount() {
        return 0;
    }

    @Override
    protected BaseViewHolder provideViewHolder(View view, int viewType) {
        return null;
    }

    @Override
    protected BaseItem provideItem(int position) {
        return null;
    }
}
