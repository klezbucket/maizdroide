package py.com.klezlab.maizdroide.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import py.com.klezlab.maizdroide.fragment.BaseFragment;

public abstract class BaseTabFragmentAdapter extends FragmentStatePagerAdapter {
    private final ArrayList<BaseFragment> fragments = new ArrayList<>();
    private final Context context;

    public BaseTabFragmentAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        context = ctx;

        final ArrayList<String> tags = provideFragmentTags();
        int i = 0;

        for (String tag : tags) {
            BaseFragment fragment = resolvFragmentByTag(fm,tag,i);
            fragments.add(fragment);
            i++;
        }
    }

    @NonNull
    private BaseFragment resolvFragmentByTag(FragmentManager fm, String tag, int i) {
        final List<Fragment> frags = fm.getFragments();

        if(frags != null){
            for(Fragment frag : frags){
                BaseFragment baseFragment = (BaseFragment) frag;

                if(baseFragment.provideFragmentTag().equals(tag)){
                    return baseFragment;
                }
            }
        }

        return provideFragment(i);
    }


    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    public Fragment getFragment(int position){
        return getItem(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return provideTabTitle(context,position);
    }

    @NonNull protected abstract ArrayList<String> provideFragmentTags();
    @NonNull protected abstract BaseFragment provideFragment(int position);
    public abstract int provideTabIcon(int position);
    public abstract CharSequence provideTabTitle(Context context, int position);
}