package py.com.klezlab.maizdroide.persistent;

import android.content.Context;

public class Persistent {
    public static PersistentImpl with(Context ctx,String name){
        return new PersistentImpl(ctx,name);
    }
}
