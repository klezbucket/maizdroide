package py.com.klezlab.maizdroide.persistent;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import py.com.klezlab.maizdroide.debug.Logcat;

public class PersistentImpl {
    private Context ctx;
    private String name;

    public PersistentImpl(Context ctx, String name){
        this.ctx = ctx;
        this.name = name;
    }

    public Map<String,?> keys(){
        SharedPreferences prefs = getPreferences();
        return prefs.getAll();
    }

    public void write(String key, String val){
        SharedPreferences prefs = getPreferences();
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, val);

        if(val == null) {
            Logcat.debug(name.toUpperCase() + " | Write<" + key + "> : <null> (hardcoded log)");
        }
        else {
            Logcat.debug(name.toUpperCase() + " | Write<" + key + "> : <" + val + ">");
        }

        if(edit.commit() == false){
            Logcat.debug(name.toUpperCase() + " | Commit Failed");
        }
    }

    public void write(String key, long val){
        SharedPreferences prefs = getPreferences();
        SharedPreferences.Editor edit = prefs.edit();
        edit.putLong(key, val);

        Logcat.debug(name.toUpperCase() + " | Write<" + key + "> : <" + val + ">");

        if(edit.commit() == false){
            Logcat.debug(name.toUpperCase() + " | Commit Failed");
        }
    }

    public void write(String key, boolean val){
        SharedPreferences prefs = getPreferences();
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(key, val);

        Logcat.debug(name.toUpperCase() + " | Write<" + key + "> : <" + val + ">");

        if(edit.commit() == false){
            Logcat.debug(name.toUpperCase() + " | Commit Failed");
        }
    }


    public long readLong(String key){
        SharedPreferences prefs = getPreferences();
        long val = prefs.getLong(key, 0);
        Logcat.debug(name.toUpperCase() + " | Read<" + key + "> : <" + val + ">");

        return val;
    }

    public boolean readBoolean(String key){
        SharedPreferences prefs = getPreferences();
        boolean val = prefs.getBoolean(key, false);
        Logcat.debug(name.toUpperCase() + " | Read<" + key + "> : <" + val + ">");

        return val;
    }

    public String read(String key){
        SharedPreferences prefs = getPreferences();
        String val = prefs.getString(key, null);

        if(val == null) {
            Logcat.debug(name.toUpperCase() + " | Read<" + key + "> : <null> (hardcoded log)");
        }
        else{
            Logcat.debug(name .toUpperCase()+ " | Read<" + key + "> : <" + val + ">");

        }

        return val;
    }

    private SharedPreferences getPreferences(){
        return ctx.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    public int readInt(String key) {
        SharedPreferences prefs = getPreferences();
        int val = (int) prefs.getLong(key, 0);
        Logcat.debug(name.toUpperCase() + " | Read<" + key + "> : <" + val + ">");

        return val;
    }
}
