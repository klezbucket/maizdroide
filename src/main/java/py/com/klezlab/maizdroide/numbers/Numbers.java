package py.com.klezlab.maizdroide.numbers;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Numbers {
    public static int integerize(Object o){
        if(o == null){
            return 0;
        }

        int i = 0;

        try {
            if (o instanceof String) {
                final String s = (String) o;
                i = Integer.parseInt(s);
            }
            else if (o instanceof Double) {
                final Double d = (double) o;
                i = d.intValue();
            }
        }
        catch(Exception e){
            Logcat.exception("Exception @ Numbers::integerze()",e);
            i = 0;
        }

        return i;
    }
}
