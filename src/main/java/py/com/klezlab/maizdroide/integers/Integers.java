package py.com.klezlab.maizdroide.integers;

import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Integers {
    @Nullable
    public static int extractor(Object object){
        if(object == null){
            return 0;
        }

        int value = 0;

        try {
            if (object instanceof String) {
                final String s = (String) object;
                value = Integer.parseInt(s);
            } else if (object instanceof Double) {
                final double d = (double) object;
                value = (int) d;
            } else if (object instanceof Integer) {
                final int i = (int) object;
                value = i;
            } else if (object instanceof Boolean) {
                final boolean b = (boolean) object;
                value = b ? 1 : 0;
            }
        }
        catch (Exception e){
            Logcat.exception("Exception @ Integers::extractor()",e);
        }

        return value;
    }
}