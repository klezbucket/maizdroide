package py.com.klezlab.maizdroide.form;

import android.view.View;

abstract public class Field {
    private String value;
    private String name;

    public Field(String name, String value) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract View getView();
    public abstract void apply();
    public abstract String extract();
}
