package py.com.klezlab.maizdroide.form;

import android.view.View;
import android.widget.Spinner;

import py.com.klezlab.maizdroide.adapter.SpinnerAdapter;
import py.com.klezlab.maizdroide.item.SpinnerItem;

public class SelectField extends Field{
    private Spinner spinner;

    public SelectField(Spinner s, String name, String value) {
        super(name, value);
        this.spinner = s;
    }

    @Override
    public View getView() {
        return spinner;
    }

    public Spinner getSpinner() {
        return spinner;
    }

    @Override
    public void apply() {
        SpinnerAdapter adapter = (SpinnerAdapter) spinner.getAdapter();
        int position = adapter.getPosition(getValue());
        spinner.setSelection(position);
    }

    @Override
    public String extract() {
        int position = spinner.getSelectedItemPosition();
        SpinnerAdapter adapter = (SpinnerAdapter) spinner.getAdapter();
        SpinnerItem item = adapter.getSpinnerItem(position);
        return item.getValue();
    }
}