package py.com.klezlab.maizdroide.actionbar;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import py.com.klezlab.maizdroide.activity.MaizActivity;

public abstract class ActionBarAbstractEngine implements ActionBarHook {
    private ActionBar actionbar;
    private Menu menu;
    private MaizActivity activity;

    public void showMenu(int id) {
        toggleMenu(id,true);
    }

    public void hideMenu(int id) {
        toggleMenu(id,false);
    }

    private void toggleMenu(int id, boolean b) {
        if(menu == null){
            return;
        }

        if(activity == null){
            return;
        }

        final MenuItem item = menu.findItem(id);

        if(item == null){
            return;
        }

        item.setVisible(b);
        item.setEnabled(b);

        activity.onPrepareOptionsMenu(menu);
    }

    @Override
    public void atCreate(MaizActivity activity, Bundle savedInstanceState) {
        final Toolbar toolbar = (Toolbar) activity.findViewById(provideToolbar());
        String title = provideTitle();

        if(title == null) {
            title = "";
        }

        if (toolbar != null) {
            toolbar.setTitle(title);
            activity.setSupportActionBar(toolbar);
        }

        actionbar = activity.getSupportActionBar();

        if(actionbar != null) {
            actionbar.setHomeButtonEnabled(isHomeButtonEnabled());
            actionbar.setDisplayHomeAsUpEnabled(isHomeButtonEnabled());

            final int drawable = provideHomeDrawable();
            actionbar.setHomeAsUpIndicator(drawable);
        }
    }

    @Override
    public void atStop(MaizActivity activity) {

    }

    @Override
    public void atResume(MaizActivity activity) {

    }

    @Override
    public void atSaveInstanceState(MaizActivity activity, Bundle outstate) {

    }

    @Override
    public void atStart(MaizActivity activity) {
    }

    @Override
    public void atCreateOptionsMenu(MaizActivity activity, Menu menu) {
        this.menu = menu;
        this.activity = activity;

        activity.getMenuInflater().inflate(provideMenu(), menu);
    }

    @Override
    public boolean atOptionsItemSelected(MaizActivity activity, MenuItem item) {
        if(atMenuCallback(activity,item)){
            return true;
        }

        return false;
    }

    public void setToolbarTitle(String title){
        if(actionbar == null){
            return;
        }

        if(title == null){
            title = "";
        }

        actionbar.setTitle(title);
    }

    @Override
    public void atActivityResult(MaizActivity activity, int requestCode, int resultCode, Intent data) {

    }

    @Nullable
    public Menu getMenu() {
        return menu;
    }
}
