package py.com.klezlab.maizdroide.actionbar;

import android.view.MenuItem;

import py.com.klezlab.maizdroide.activity.Hook;
import py.com.klezlab.maizdroide.activity.MaizActivity;

public interface ActionBarHook extends Hook {
    int provideHomeDrawable();
    boolean atMenuCallback(MaizActivity activity, MenuItem item);
    boolean isHomeButtonEnabled();
    String provideTitle();
    int provideToolbar();
    int provideMenu();
}
