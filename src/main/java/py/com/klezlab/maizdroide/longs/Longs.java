package py.com.klezlab.maizdroide.longs;

import android.support.annotation.Nullable;

import py.com.klezlab.maizdroide.debug.Logcat;

public class Longs {
    @Nullable
    public static long extractor(Object object){
        if(object == null){
            return 0;
        }

        long value = 0;

        try {
            if (object instanceof String) {
                final String s = (String) object;
                value = Long.parseLong(s);
            } else if (object instanceof Double) {
                final double d = (double) object;
                value = (long) d;
            } else if (object instanceof Integer) {
                final long i = (long) object;
                value = i;
            } else if (object instanceof Boolean) {
                final boolean b = (boolean) object;
                value = b ? 1 : 0;
            }
        }
        catch (Exception e){
            Logcat.exception("Exception @ Longs::extractor()",e);
        }

        return value;
    }
}